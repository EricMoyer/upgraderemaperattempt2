# UpgradeRemaperAttempt2

Place for me to put my second attempt to try to upgrade the remapper.

It contains a python version. But it runs out of memory on the full
dataset. It is also quite slow since it can't use multiprocessing.

I rewrote the python version as a C++ version using C++17 and the [gnu
parallel extensions][1]. Run make to compile it.

# Important
NOTE: this uses submodules - so run
    git submodule update --init --recursive
after downloading


[1]: https://gcc.gnu.org/onlinedocs/libstdc++/manual/parallel_mode_using.html
