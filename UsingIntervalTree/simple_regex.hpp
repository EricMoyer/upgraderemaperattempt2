#include<regex>

///\brief Allows you to do simple "match/does not match" calculations easily
///
/// Hides the complexity introduced by the C++ library's passion for
/// efficiency. Example: has_xes = match(".*xx.*", my_str)
template<class PatternType>
inline bool match(const PatternType& pattern, const std::string& str) {
    const std::regex pat(pattern);
    std::smatch match_info;
    return std::regex_match(str, match_info, pat);
}
