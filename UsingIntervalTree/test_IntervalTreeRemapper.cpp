#include "IntervalTreeRemapper.hpp"
#include "catch2_test_framework.hpp"

TEST_CASE("opposite(strand)", "[remapper_internals]") {
    CHECK(opposite(Strand::pos) == Strand::neg);
    CHECK(opposite(Strand::neg) == Strand::pos);
}

TEST_CASE("SimplifiedEdge from parts", "[remapper_internals]") {
    auto e = SimplifiedEdge::from(-1, 7, 12, +1);
    CHECK(e.target_strand() == Strand::pos);
    CHECK(e.source_strand() == Strand::neg);
    CHECK(e.target_id() == 7);
    CHECK(e.target_start() == 12);
}

TEST_CASE("SimplifiedEdge operator<", "[remapper_internals]") {
    // Note: I don't care what the order is here .. just that
    // different things are not equivalent and equal things are
    // equivalent
    std::vector<SimplifiedEdge> e{
        SimplifiedEdge::from(-1, 7, 12, +1),
        SimplifiedEdge::from(1, 7, 12, +1),
        SimplifiedEdge::from(-1, 8, 12, +1),
        SimplifiedEdge::from(-1, 7, 13, +1),
        SimplifiedEdge::from(-1, 7, 12, -1)};
    unsigned i,j;
    for(i = 0; i < e.size(); ++i) {
        for(j = 0; j < e.size(); ++j) {
            if(i == j) {
                CHECK_FALSE(e[i] < e[j]);
                CHECK_FALSE(e[j] < e[i]);
            } else {
                /// Equivalent to CHECK(e[i] < e[j] || e[j] < e[i])
                /// CATCH2 cannot handle the above expression even with
                /// parentheses
                if(e[i] < e[j]) {
                    CHECK(e[i] < e[j]);
                } else {
                    CHECK(e[j] < e[i]);
                }
            }
        }
    }
}

TEST_CASE("PtRemappingEdge from string and op<<", "[remapper_internals]") {
    using PtEdge = IntervalTreeRemapper::PtRemappingEdge;
    std::string as_string = "-23[2,7)>+7@12";
    PtEdge as_edge = PtEdge::from(as_string);
    std::ostringstream as_string_again; as_string_again << as_edge;
    CHECK(as_string_again.str() == as_string);
    CHECK(as_edge.id == 23);
    CHECK(as_edge.start == 2);
    CHECK(as_edge.supremum == 7);
    CHECK(as_edge.s_edge.source_strand() == Strand::neg);
    CHECK(as_edge.s_edge.target_strand() == Strand::pos);
    CHECK(as_edge.s_edge.target_id() == 7);
    CHECK(as_edge.s_edge.target_start() == 12);
}

TEST_CASE("construct empty tree","[remapper]") {
    std::vector<Edge> empty;
    IntervalTreeRemapper m(empty.begin(), empty.end());
    CHECK(m.to_string() == R"({"seqids":[], "trees":[]})");
}

TEST_CASE("construct tree with 1 edge connecting 2 seqids", "[remapper]") {
    std::vector<Edge> one {
        Edge{Edge::from_string("NC1.1:10:+:10\tNC1.2:10:+:10")}
    };
    IntervalTreeRemapper m(one.begin(), one.end());

    const auto& e = one[0];
    CHECK(m.contains(e));
    CHECK(m.contains(e.reversed()));
    CHECK(m.contains(e.from.id));
    CHECK(m.contains(e.to.id));

    Edge empty{Edge::from_string("NC1.1:10:+:0\tNC1.2:10:+:0")};
    CHECK_FALSE(m.contains(empty));
    CHECK_FALSE(m.contains(empty.reversed()));


    Edge bad{Edge::from_string("NC1.1:10:+:10\tNC1.3:10:+:10")};
    CHECK_FALSE(m.contains(bad));
    CHECK_FALSE(m.contains(bad.reversed()));
    CHECK(m.contains(bad.from.id));
    CHECK_FALSE(m.contains(bad.to.id));

    // Test to_string
    CHECK(m.to_string() == "{\"seqids\":[\"NC1.2\", \"NC1.1\"], \"trees\":[[\"NC1.2:10:+:10\tNC1.1:10:+:10\"], [\"NC1.1:10:+:10\tNC1.2:10:+:10\"]]}");

}


TEST_CASE("construct tree with 2 edges connecting 2 seqids", "[remapper]") {
    std::vector<Edge> two {
        Edge{Edge::from_string("NC1.1:10:+:10\tNC1.2:10:+:10")},
        Edge{Edge::from_string("NC1.1:12:+:10\tNC1.2:12:-:10")}
    };
    IntervalTreeRemapper m(two.begin(), two.end());

    for(const auto& e: two) {
        CHECK(m.contains(e));
        CHECK(m.contains(e.reversed()));
        CHECK(m.contains(e.from.id));
        CHECK(m.contains(e.to.id));
    }

    Edge bad{Edge::from_string("NC1.1:10:+:10\tNC1.3:10:+:10")};
    CHECK_FALSE(m.contains(bad));
    CHECK_FALSE(m.contains(bad.reversed()));
    CHECK(m.contains(bad.from.id));
    CHECK_FALSE(m.contains(bad.to.id));
}



TEST_CASE("construct tree with 2 edges connecting 4 seqids", "[remapper]") {
    std::vector<Edge> two {
        Edge{Edge::from_string("NC1.1:10:+:10\tNC1.2:10:+:10")},
        Edge{Edge::from_string("NC2.1:12:+:10\tNC2.2:12:-:10")}
    };
    IntervalTreeRemapper m(two.begin(), two.end());

    for(const auto& e: two) {
        CHECK(m.contains(e));
        CHECK(m.contains(e.reversed()));
        CHECK(m.contains(e.from.id));
        CHECK(m.contains(e.to.id));
    }

    Edge bad{Edge::from_string("NC1.1:10:+:10\tNC1.3:10:+:10")};
    CHECK_FALSE(m.contains(bad));
    CHECK_FALSE(m.contains(bad.reversed()));
    CHECK(m.contains(bad.from.id));
    CHECK_FALSE(m.contains(bad.to.id));
}


TEST_CASE("simplified edge from empty edge", "[remapper]") {
    Edge empty{Edge::from_string("NC1.1:10:+:0\tNC1.2:10:+:0")};
    SeqIdTable table{empty.from.id, empty.to.id};
    CHECK_NOTHROW(SimplifiedEdge::from(empty, table));
}

TEST_CASE("simplified edge inc_target_start", "[remapper]"){
   Edge pos{Edge::from_string("NC1.1:10:+:2\tNC1.2:20:+:2")};
   SeqIdTable table{pos.from.id, pos.to.id};

   auto p = SimplifiedEdge::from(pos, table);
   CHECK(p.target_start() == 20);
   p.inc_target_start(4);
   CHECK(p.target_start() == 24);
   CHECK(p.target_strand() == Strand::pos);

   Edge neg{Edge::from_string("NC1.1:10:+:2\tNC1.2:20:-:2")};

   auto n = SimplifiedEdge::from(neg, table);
   CHECK(n.target_start() == 20);
   n.inc_target_start(4);
   CHECK(n.target_start() == 24);
   CHECK(n.target_strand() == Strand::neg);
 }

TEST_CASE("IntSeqIdAndInterval", "[remapper]") {
    using Catch::Matchers::Contains;
    Edge empty{Edge::from_string("NC1.1:10:+:0\tNC1.2:10:+:0")};
    Edge nonempty{Edge::from_string("NC1.1:10:+:1\tNC1.2:20:+:1")};
    SeqIdTable table{empty.from.id, empty.to.id};

    CHECK_NOTHROW(IntSeqIdAndInterval{nonempty, table});
    CHECK_THROWS_WITH((IntSeqIdAndInterval{empty, table}), Contains("empty"));
}

TEST_CASE("edges_for_remapping_first_point_of one-edge", "[remapper]") {
    std::vector<Edge> one {
        Edge{Edge::from_string("NC1.1:10:+:10\tNC1.2:20:+:10")},
    };
    IntervalTreeRemapper m(one.begin(), one.end());

    auto map = [&m](std::string interval) {
        auto I = Interval::from_string;
        return m.edges_for_remapping_first_point_of(I(interval));
    };
    using PME=IntervalTreeRemapper::PtRemappingEdge;

    auto m_before = map("NC1.1:9:+:1");
    CHECK(m_before.edges.empty());
    CHECK(m_before.min_len == 1);

    auto m_b4overlap = map("NC1.1:9:+:2");
    CHECK(m_b4overlap.edges.empty());
    CHECK(m_b4overlap.min_len == 2);

    {
        auto m_on = map("NC1.1:10:+:1");
        std::set<PME> expected_edges {PME::from("+1[10,20)>+0@20")};
        CHECK(m_on.edges == expected_edges);
        CHECK(m_on.min_len == 1);
    }


    {
        auto m_on_l3 = map("NC1.1:10:+:3");
        std::set<PME> expected_edges {PME::from("+1[10,20)>+0@20")};
        CHECK(m_on_l3.edges == expected_edges);
        CHECK(m_on_l3.min_len == 3);
    }

    {
        auto m_after_start = map("NC1.1:11:+:3");
        std::set<PME> expected_edges {PME::from("+1[11,20)>+0@21")};
        CHECK(m_after_start.edges == expected_edges);
        CHECK(m_after_start.min_len == 3);
    }

    auto m_after_all = map("NC1.1:20:+:1");
    CHECK(m_after_all.edges.empty());
    CHECK(m_after_all.min_len == 1);

    {
        auto m_reverse_on = map("NC1.2:20:+:2");
        std::set<PME> expected_edges {PME::from("+0[20,30)>+1@10")};
        CHECK(m_reverse_on.edges == expected_edges);
        CHECK(m_reverse_on.min_len == 2);
    }
}

struct Checker {
    struct Example {
        std::string to_map;
        std::vector<std::string> edges;
        unsigned min_len;
    };

    const IntervalTreeRemapper* m;
    Checker(IntervalTreeRemapper* m):m(m){REQUIRE(m != nullptr);}
    void check(const Example& e) const{
        using PME=IntervalTreeRemapper::PtRemappingEdge;
        auto I = Interval::from_string;
        auto mapped=m->edges_for_remapping_first_point_of(I(e.to_map));
        std::set<PME> expected_edges;
        std::transform(e.edges.begin(), e.edges.end(),
                       std::inserter(expected_edges, expected_edges.end()),
                       static_cast<PME(&)(std::string)>(PME::from));
        CHECK(mapped.edges == expected_edges);
        CHECK(mapped.min_len == e.min_len);
    }
};

TEST_CASE("edges_for_remapping_first_point_of empty interval", "[remapper]") {
    using Catch::Matchers::Contains;
    std::vector<Edge> one {
        Edge{Edge::from_string("NC1.1:10:+:20\tNC1.1:20:+:20")},
    };
    IntervalTreeRemapper m(one.begin(), one.end());
    auto I = Interval::from_string;
    CHECK_THROWS_WITH(m.edges_for_remapping_first_point_of
                        (I("NC1.1:10:+:0")), Contains("empty"));
}

TEST_CASE("edges_for_remapping_first_point_of edge mapping to itself", "[remapper]") {
    std::vector<Edge> one {
        Edge{Edge::from_string("NC1.1:10:+:20\tNC1.1:20:+:20")},
    };
    IntervalTreeRemapper m(one.begin(), one.end());
    const Checker checker(&m);

    auto check = [&checker](const Checker::Example& e){
        checker.check(e);
    };

    //Before
    check({"NC1.1:9:+:2", {}, 2});
    //In
    check({"NC1.1:10:+:1", {}, 1});
    //After
    check({"NC1.1:30:+:1", {}, 1});
}

TEST_CASE("edges_for_remapping_first_point_of 2 edges for chain of 3 sequences ", "[remapper]") {
    std::vector<Edge> one {
        Edge{Edge::from_string("NC1.1:10:+:20\tNC1.2:20:+:20")},
        Edge{Edge::from_string("NC1.2:25:+:10\tNC1.3:35:+:10")},
    };
    IntervalTreeRemapper m(one.begin(), one.end());
    const Checker checker(&m);

    auto check = [&checker](const Checker::Example& e){
        checker.check(e);
    };

    /////
    // Mappings from NC1.1
    /////

    // Before First point no remappings despite overlap with real first pt
    check({"NC1.1:9:+:2", {}, 2});
    // First point - it only remaps to one sequence
    check({"NC1.1:10:+:1", {"+2[10,30)>+1@20"}, 1});
    // First point with extension that runs into the overlapping
    // area -- it only remaps to one sequence
    check({"NC1.1:11:+:10", {"+2[11,30)>+1@21"}, 10});
    // First point in overlap region
    check({"NC1.1:15:+:1", {"+2[15,30)>+1@25", "+1[25,35)>+0@35"}, 1});
    // Last point in overlap region plus a little
    check({"NC1.1:24:+:2", {"+2[24,30)>+1@34", "+1[34,35)>+0@44"}, 1});
    // First point outside overlap region
    check({"NC1.1:25:+:1", {"+2[25,30)>+1@35"}, 1});

    /////
    // Mappings from NC1.2
    /////

    // Before First point - no remapping edges
    check({"NC1.2:19:+:2", {}, 2});
    // First point - it only remaps to one sequence
    check({"NC1.2:20:+:1", {"+1[20,40)>+2@10"}, 1});
    // First point with extension that runs into the overlapping
    // area -- it only remaps to one sequence
    check({"NC1.2:21:+:10", {"+1[21,40)>+2@11"}, 10});
    // First point in overlap region
    check({"NC1.2:25:+:1", {"+1[25,40)>+2@15", "+1[25,35)>+0@35"}, 1});
    // Last point in overlap region plus a little
    check({"NC1.2:34:+:2", {"+1[34,40)>+2@24", "+1[34,35)>+0@44"}, 1});
    // First point outside overlap region
    check({"NC1.2:35:+:1", {"+1[35,40)>+2@25"}, 1});

    /////
    // Mappings from NC1.3
    /////

    // Before First point - no remapping edges
    check({"NC1.3:29:+:2", {}, 2});
    // First point - it does not remap from NC1.3
    check({"NC1.3:30:+:1", {}, 1});
    // First point with extension that runs into the overlapping
    // area - but still does not remap from NC1.3
    check({"NC1.3:31:+:10", {}, 10});
    // First point in overlap region - finally a remap
    check({"NC1.3:35:+:1", {"+0[35,45)>+1@25", "+1[25,40)>+2@15"}, 1});
    // Last point in overlap region plus a little
    check({"NC1.3:44:+:2", {"+0[44,45)>+1@34", "+1[34,40)>+2@24"}, 1});
    // First point outside overlap region - doesn't remap from NC1.3
    check({"NC1.3:45:+:1", {}, 1});
}
