#pragma once
#include <algorithm>
#include <bitset>
#include <cstdint>
#include <ctime>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <experimental/optional>
#include <parallel/algorithm>
#include <set>
#include <sstream>
#include <string>
#include <tuple>

namespace par = __gnu_parallel; // Bad (but not quite as bad)

constexpr bool DEBUG{false};

/// Current local time as a string
inline std::string now_str() {
    using namespace std;
    auto t = time(nullptr);
    auto tm = localtime(&t);
    ostringstream s;
    s << put_time(tm, "%Y-%m-%dT%H:%M:%S");
    return s.str();
}

/// String split from https://stackoverflow.com/a/236803/309334
template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

/// String split from https://stackoverflow.com/a/236803/309334
inline std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

/// Join a sequence of items stringifiable by operator<< using delim
/// between them
template<class InputIter>
std::string join(InputIter begin, InputIter end, const std::string& delim=", ") {
    std::ostringstream out;
    const std::string empty = "";
    const std::string* d=&empty;
    while(begin != end){
        out << *d << *begin++;
        d=&delim;
    }
    return out.str();
}

struct SeqId {
    /// The accession part of the seq id
    std::string acc;
    /// Version ... 0 if absent
    unsigned char ver;

    static SeqId from_string(const std::string& s){
        using namespace std;
        auto fields = split(s, '.');
        if (fields.size() > 1){
            return SeqId{fields.at(0), static_cast<unsigned char>(stoul(fields.at(1)))};
        }else{
            return SeqId{fields.at(0), 0};
        }
    }

    /// The extra priority given to the accession because of its special form
    ///
    /// Accessions matching regexes get the given priority
    /// 0 ^NC_
    /// 1 ^N._
    /// 2 ^.._
    /// 3 ^.*
    int priority() const{
        if (acc.size() >= 3 and acc[2] == '_'){
            if (acc.size() >= 1 and acc[0] == 'N') {
                if(acc.size() >= 2 and acc[1] == 'C') {
                    return 0;
                } else {
                    return 1;
                }
            } else {
                return 2;
            }
        } else {
            return 3;
        }
    }

    /// This is an approximate PTLP ordering
    ///
    /// Sort as tuple of priority, accession and version except:
    /// version is reversed (so larger versions of the same sequence go first).
    bool operator<(const SeqId& s) const {
        using namespace std;
        auto p = priority(), sp = s.priority();
        return tie(p, acc, s.ver) < tie(sp, s.acc, ver);
    }

    bool operator==(const SeqId& s) const {
        using namespace std;
        return tie(acc, s.ver) == tie(s.acc, ver);
    }


    std::string to_string() const {
        using namespace std;
        int v = ver;
        if(v > 0){
            return acc + "." + std::to_string(static_cast<int>(ver));
        }else{
            return acc;
        }
    }
};

inline std::ostream& operator<<(std::ostream& out, const SeqId& id) {
    return out << id.to_string();
}

/// Sorted list of SeqIds
///
/// Must be in PTLP order
using SeqIdTable = std::vector<SeqId>;

/// Typedef to make clear when I'm expecting a seqid encoded as an
/// integer index to some SeqIdTable.
using IntSeqId = uint32_t;

/// Special value to indicate IntSeqId not in table
///
/// TODO: put bad_idx in a namespace and rename to something like bad_int_seq_id
constexpr IntSeqId bad_idx = std::numeric_limits<IntSeqId>::max();

/// Return the index of the SeqId in the table
///
/// returns bad_idx if the value is not found
///
/// This will actually fit in 19 bits ...
inline IntSeqId seqid_index_of(const SeqIdTable& t, const SeqId& id){
    using namespace std;
    auto it = lower_bound(t.begin(), t.end(), id);
    if (it == t.end() or !(*it == id)){
        return bad_idx;
    } else {
        return it - t.begin();
    }
}

struct Interval {
    /// The seq id for the interval
    SeqId id;

    /// First position possibly in the interval.
    uint32_t start;

    /// First position after the interval (= start for empty interval)
    /// Guaranteed >= start
    uint32_t supremum;

    /// '+' for positive strand, '-' for negative strand
    char strand;

    /// Number of positions in the interval
    uint32_t length() const {
        return supremum - start;
    }

    bool empty() const {
        return length() == 0;
    }

    /// Return the sub-interval of this interval that lies strictly after \a i
    ///
    /// Return empty optional if no portion of this interval lies after \a i
    ///
    /// Note that this code ignores sequence and strand.
    std::experimental::optional<Interval> after(const Interval& i) const {
        // If i ends after this sequence, there is nothing after
        if(i.supremum > supremum) {
            return {};
        } else {
            return {Interval{id, i.supremum, supremum, strand}};
        }
    }

    static Interval from_string(const std::string& s){
        auto fields = split(s, ':');
        uint32_t start = stoul(fields.at(1));
        uint32_t len = stoul(fields.at(3));
        return Interval{
            SeqId::from_string(fields.at(0)),
            start, start+len, fields.at(2)[0]};
    }

    /// Compare as a tuple
    /// Note that supremum is reverse-ordered so longer intervals go first
    bool operator<(const Interval& i) const {
        using namespace std;
        return tie(id, start, i.supremum, strand) <
            tie(i.id, i.start, supremum, i.strand);
    }

    bool operator==(const Interval& i) const {
        using namespace std;
        return tie(id, start, i.supremum, strand) ==
            tie(i.id, i.start, supremum, i.strand);
    }

    std::string to_string() const {
        return id.to_string() + ":" + std::to_string(start) + ":" +
            strand + ":" + std::to_string(length());
    }
};

inline std::ostream& operator<<(std::ostream& out, Interval i) {
    return out << i.to_string();
}


struct Edge {
    Interval from;
    Interval to;

    static Edge from_string(const std::string& s) {
        const auto& fields = split(s, '\t');
        return Edge{
            Interval::from_string(fields.at(0)),
            Interval::from_string(fields.at(1))};
    }

    Edge sorted() const {
        return from < to ? *this : reversed();
    }

    Edge reversed() const {
        return Edge{to, from};
    }

    bool operator<(const Edge&e) const {
        return std::tie(from, to) < std::tie(e.from, e.to);
    }

    bool operator==(const Edge&e) const {
        return std::tie(from, to) == std::tie(e.from, e.to);
    }

    std::string to_string() const {
        return from.to_string() + "\t" + to.to_string();
    }
};

inline std::ostream& operator<<(std::ostream& out, Edge e) {
    return out << e.to_string();
}
