#pragma once
#define USE_INTERVAL_TREE_NAMESPACE
#include "c_interval_tree_remapping_lib.hpp"
#include "submodules/intervaltree/IntervalTree.h"
#include <cstdint>
#include <cstdlib>
#include <deque>
#include <map>
#include <set>

/// Strand of a double-stranded molecule.
///
/// It is assumed that it only will have these values. Don't cast or
/// use list initialization to give another value.
enum class Strand {
    // Positive strand
    pos = 1,
    // Negative strand
    neg = -1,
};

/// Return the opposite strand
Strand opposite(Strand s){
    return static_cast<Strand>(-1 * static_cast<int>(s));
}

/// An edge where the source and destination are the same length and
/// the source interval (though not strand) is stored in the
/// containing object and where the target id can be specified with an
/// integer.
struct SimplifiedEdge {
    /// Absolute value - 1 is the 0-based interbase coordinate of the
    /// target interval start on the target sequence. Positive for
    /// positive strand, negative for negative strand.
    ///
    /// Note that the -1 is needed because we can't store 0 and
    /// have the sign be valid.
    ///
    /// Note: these are technically public but pretend we're using Python
    int32_t _start_and_strand;
    /// Absolute value - 1 is ihe id of the target sequence ... its
    /// index in _seqids. The sign is the strand of the source
    /// sequence.
    ///
    /// Note: these are technically public but pretend we're using Python
    int32_t _seqid_and_source_strand;

    bool operator==(const SimplifiedEdge& rhs) const {
        return _start_and_strand == rhs._start_and_strand &&
            _seqid_and_source_strand == rhs._seqid_and_source_strand;
    }

    bool operator<(const SimplifiedEdge& rhs) const {
        return std::tie(_start_and_strand, _seqid_and_source_strand) <
             std::tie(rhs._start_and_strand, rhs._seqid_and_source_strand);
    }

    /// Implementation of signum function from
    /// https://stackoverflow.com/a/1903975/309334
    static inline int32_t sgn(int32_t in){
        return (in > 0) - (in < 0); // branchless?
    }

    uint32_t target_start() const {
        return std::abs(_start_and_strand) - 1;
    }

    Strand target_strand() const {
        return static_cast<Strand>(sgn(_start_and_strand));
    }

    uint32_t target_id() const {
        return std::abs(_seqid_and_source_strand) - 1;
    }

    Strand source_strand() const {
        return static_cast<Strand>(sgn(_seqid_and_source_strand));
    }

    void set_source_strand(Strand s) {
        _seqid_and_source_strand = static_cast<int>(s) *
            std::abs(_seqid_and_source_strand);
    }

    /// Increment the start position on the target strand by delta
    void inc_target_start(uint32_t delta) {
        if(_start_and_strand > 0) {
            _start_and_strand += delta;
        } else {
            _start_and_strand -= delta;
        }
    }

    bool operator==(const SimplifiedEdge& rhs) {
        return _start_and_strand == rhs._start_and_strand &&
            _seqid_and_source_strand == rhs._seqid_and_source_strand;
    }

    /// Return a SimplifiedEdge from components
    ///
    ///
    /// \param source_strand +1 or -1 for positive or negative
    ///                      respectively
    ///
    /// \param target_id An IntSeqId ... but make sure it is in range
    ///                  before calling.
    ///
    /// \param target_start start position on the target
    ///
    ///
    /// \param target_strand like source strand (but for the target
    ///                      sequence)
    static SimplifiedEdge from(int source_strand, int32_t target_id,
                               int32_t target_start, int target_strand) {
        return SimplifiedEdge{(target_start + 1)*target_strand,
                              (target_id + 1)*source_strand};
    }

    static SimplifiedEdge from(const Edge& e, const SeqIdTable& t) {
        auto source_strand = e.from.strand == '+' ? 1 : -1;

        auto target_id_unsigned = seqid_index_of(t, e.to.id);
        if (target_id_unsigned + 1 > std::numeric_limits<int32_t>::max()) {
            //LCOV_EXCL_START -- to make this happen we'd need 4 billion seq_ids
            std::cerr << "seqid + 1 does not fit in 31 bits " << e.to.id
                      << "becomes " << seqid_index_of(t, e.to.id) << "\n";
            std::exit(-1);
            //LCOV_EXCL_STOP
        }
        int32_t target_id = static_cast<int32_t>(target_id_unsigned);

        int32_t target_start = e.to.start; // can't be out of range
                                           // because largest human
                                           // sequence (chromosome 1)
                                           // only has 249 million nt
        auto target_strand = e.to.strand == '+' ? 1 : -1;
        return SimplifiedEdge{(target_start + 1)*target_strand,
                              (target_id + 1)*source_strand};
    }
};



/// Return the index \a e.from.id in \a t and an interval
/// corresponding to e.from which has e.to and strand information
/// encoded in its SimplifiedEdge data.
struct IntSeqIdAndInterval {
    using IntervalWithEdge = interval_tree::Interval<uint32_t, SimplifiedEdge>;
    IntSeqId id;
    IntervalWithEdge interval;

    IntSeqIdAndInterval(const Edge& e, const SeqIdTable& t):
        id{seqid_index_of(t, e.from.id)}, interval{make_interval(e, t)} { }
private:
    IntervalWithEdge make_interval(const Edge&e, const SeqIdTable& t) {
        auto start = e.from.start;
        auto end = e.from.supremum;
        if (end <= start) {
            throw std::runtime_error("Attempt to make an interval_tree::Interval "
                                     "(which must be closed and non-empty from an "
                                     "empty interval: " + e.to_string());
        }
        return {start, end-1, SimplifiedEdge::from(e, t)};
    }
};

class IntervalTreeRemapper {
public:
    /// Create a remapper from an iterable of Edge objects
    /// The edge objects will be copied
    template<class FwdIter>
    IntervalTreeRemapper(FwdIter begin, FwdIter end) {
        // Initialize _seqids with sorted list of all possible sequences
        {
            std::set<SeqId> sequences;
            for(auto cur = begin; cur != end; ++cur){
                sequences.insert(cur->from.id);
                sequences.insert(cur->to.id);
            }
            _seqids.reserve(sequences.size());
            std::copy(sequences.begin(), sequences.end(), std::back_inserter(_seqids));
        }

        {
            // After initialization pre_tree[i] will contain the list of
            // SimplifiedEdges with their associated interval for SeqId i.
            std::vector<std::vector<
                            interval_tree::Interval<uint32_t, SimplifiedEdge>>>
                pre_tree(_seqids.size());
            {
                // Eliminate edges that are duplicates by creating a copy with
                // only one direction: from the "smaller" interval to the "larger"
                // interval
                std::set<Edge> unique_edges;
                for(auto cur = begin; cur != end; ++cur){
                    unique_edges.emplace(cur->sorted());
                }

                // Create simplified edges in the vectors in pre_tree according to the
                // integer seqid of the from sequence
                for(const auto& e: unique_edges){
                    auto id_interval = IntSeqIdAndInterval{e, _seqids};
                    pre_tree[id_interval.id].emplace_back(id_interval.interval);
                    if(not (e.from == e.to)){
                        id_interval = IntSeqIdAndInterval{e.reversed(), _seqids};
                        pre_tree[id_interval.id].emplace_back(id_interval.interval);
                    }
                }
            }

            _mappings.reserve(pre_tree.size());
            for(auto& interval_vec: pre_tree){
                _mappings.emplace_back(std::move(interval_vec));
            }
        }
    }

    std::string to_string() const {
        std::ostringstream out;
        out << "{\"seqids\":[";
        const char * sep = ""; // separator in list of trees and list of seqids
        for(const auto& sid: _seqids){
            out << sep << '"' << sid << '"';
            sep = ", ";
        }
        out << "], \"trees\":[";
        sep = ""; // Reset separator to empty for first in list of trees;
        for(IntSeqId id = 0; id < _mappings.size(); ++id){
            auto & tree = _mappings[id];
            std::vector<std::string> strings;
            auto add_to_strings = [this, id, &strings]
                (const interval_tree::Interval<uint32_t, SimplifiedEdge>& i){
                const auto& v = i.value;
                Interval src{_seqids[id], i.start, i.stop+1,
                             v.source_strand() == Strand::pos ? '+' : '-'};
                Interval target{_seqids[v.target_id()], v.target_start(),
                            v.target_start()+src.length(),
                            v.target_strand() == Strand::pos ? '+' : '-'};
                Edge e { src, target };
                strings.push_back('"' + e.to_string() + '"');
            };
            tree.visit_all(add_to_strings);
            out << sep << '[' << join(strings.begin(), strings.end(), ", ") << ']';
            sep = ", ";
        }
        out << "]}";
        return out.str();
    }

    /// Return true if this remapper contains any remappings for \a id
    bool contains(const SeqId& id) {
        auto index = seqid_index_of(_seqids, id);
        return index != bad_idx;
    }

    bool contains(const Edge& e) {
        // The interval tree I use can't represent empty intervals - so they
        // can't be stored there. This check happens first because other
        // constructions later assume a non-empty edge.
        if(e.from.supremum == e.from.start) {
            return false;
        }


        const auto id_interval = IntSeqIdAndInterval{e, _seqids};

        // The seq_id is not in the tree, so the edge can't be either
        if(id_interval.id == bad_idx) {
            return false;
        }

        // Get the list of intervals
        const auto& overlapping = _mappings[id_interval.id].findOverlapping
            (e.from.start, e.from.supremum - 1);

        // Return true if we find our target interval in the list,
        // false otherwise
        auto loc = std::find(overlapping.begin(), overlapping.end(),
                        id_interval.interval);
        return loc != overlapping.end();
    }

    /// Return the intervals corresponding to \a i
    ///
    /// The keys of the return value are the sub-intervals i must be
    /// broken into in order to map. The values are the corresponding
    /// intervals. The value list includes the original interval.
    ///
    /// The keys do not overlap and their union is \a i
    ///
    /// \a i cannot be empty
    std::multimap<Interval, Interval> equivalent(const Interval& i) const;

    /// Return the ptlp for each broken piece of i
    ///
    /// Each key returned by \a equivalent is a key in broken_ptlp
    /// The value of the returned map is the ptlp for that key interval.
    ///
    /// \a i cannot be empty
    std::map<Interval, Interval> broken_ptlp(const Interval& i) const {
        auto equ = equivalent(i);
        std::map<Interval, Interval> out;
        // Go to the first element with each key ... which will be the
        // ptlp since they are sorted by value. Put these first elements in the
        // output map
        for(auto it = equ.begin(); it != equ.end();
            it = equ.upper_bound(it->first)){
            auto last_elt = out.empty() ?
                out.end() :
                std::next(out.rbegin(),1).base();
            out.insert(last_elt, *it);
        }
        return out;
    }

    /////////////
    // In a perfect world, the following would all be private or would
    // be non-member functions ... but private is hard to test and
    // non-member functions is a little uglier. Maybe I'll make them
    // non-members eventually.
    //////////////


    /// The prefix of an interval requested for remapping and the list
    /// of equivalent intervals.
    struct PrefixAndEquivalent {
        Interval prefix;
        std::vector<Interval> equivalent;
    };


    /// Return a prefix of orig_interval that remaps cleanly and all
    /// the intervals equivalent to that prefix.
    ///
    /// "Remaps cleanly" means that all nucleotides remap to the same
    /// PTLP and are adjacent there and all intervening mappings also
    /// include no gaps.
    PrefixAndEquivalent equivalent_to_prefix(const Interval& orig_interval) const;

    using IntervalTreeNode = interval_tree::Interval<uint32_t, SimplifiedEdge>;

    /// An edge used in remapping a single nuclotide
    struct PtRemappingEdge {
        /// The from seq id
        IntSeqId id;
        uint32_t start;
        uint32_t supremum;

        SimplifiedEdge s_edge;

        uint32_t length() const {
            return supremum - start;
        }

        /// Sortable so it can be in associative containers
        bool operator<(const PtRemappingEdge& rhs) const {
            return std::tie(id, start, supremum, s_edge) <
                std::tie(rhs.id, rhs.start, rhs.supremum, rhs.s_edge);
        }

        /// Operator== for ease of use in tests
        bool operator==(const PtRemappingEdge& rhs) const {
            return std::tie(id, start, supremum, s_edge) ==
                std::tie(rhs.id, rhs.start, rhs.supremum, rhs.s_edge);
        }

        /// Return an edge created from an interval tree node and the
        /// sequence for which the interval tree held mappings and the
        /// associated images.
        static PtRemappingEdge from
        (IntSeqId id, const IntervalTreeNode& n){
            return PtRemappingEdge{id, n.start, n.stop+1, n.value};
        }

        /// Construct from a string of the form produced by the operator<<
        /// -23[2,7)>+7@12 is an edge from the negative strand of sequence 23
        /// starting at 2 with supremum 7, mapping to the positive strand of
        /// sequnce 7 starting at position 12.
        static PtRemappingEdge from(std::string);
    };

    struct EdgesMinLenAndPtlp {
        std::set<PtRemappingEdge> edges;
        uint32_t min_len;

        /// The source strand is positive if the original sequence is in the
        /// same orientation as the target, negative otherwise.
        ///
        /// The target information is identical to the \a s_edge
        /// member of the element of \a edges with the PTLP sequence.
        SimplifiedEdge ptlp;
    };

    /// Return all edges that are involved in remapping the first point of
    /// \a orig_interval + the minimum length of any involved interval.
    ///
    /// The edges are stored in the \a edges member and the minimum
    /// length in \a min_len.
    ///
    /// min_len = minimum of length over the original iterval and all
    ///           intervals that form one side of a returned edge.
    ///
    /// The first point of all resulting edges will be the first point
    /// of \a orig_interval
    ///
    /// (first being lowest index not most 5-prime on its strand)
    ///
    /// NOTE: this does not mean that all points in the returned
    /// intervals will remap the way the first point did. Notably, there
    /// may be an edge that overlaps the rest of the interval which will
    /// not be seen in this remapping. i.e. the following alignment
    ///
    /// foo: 01234
    /// bar: 56789
    /// baz:  ABCD
    ///
    /// foo:0 remaps to bar but not to baz but foo:1 remaps to bar and baz
    EdgesMinLenAndPtlp edges_for_remapping_first_point_of
    (const Interval& orig_interval) const;

private:
    /// All SeqIds that can be remapped ... stored separately so that
    /// we can use integer ids
    SeqIdTable _seqids;

    /// mappings[i] is the tree containing the edges that start with
    /// SeqId i in the _seqids table.
    std::vector<interval_tree::IntervalTree<uint32_t, SimplifiedEdge> > _mappings;

};

namespace interval_tree {
    template<class Scalar, class Value>
    bool operator==(const Interval<Scalar, Value>& a,
                    const Interval<Scalar, Value>& b){
        return a.start == b.start && a.stop == b.stop && a.value == b.value;
    }
    template<class Scalar, class Value>
    bool operator<(const Interval<Scalar, Value>& a,
                    const Interval<Scalar, Value>& b){
        return std::tie(a.start, a.stop, a.value) <
            std::tie(b.start, b.stop, b.value);
    }
}


inline IntervalTreeRemapper::PrefixAndEquivalent
IntervalTreeRemapper::equivalent_to_prefix(const Interval& orig_interval) const {
    auto edges = edges_for_remapping_first_point_of(orig_interval);
    // TODO: implement equvalent_to_prefix
    return {};
}

/// Printer for IntervalTreeRemapper::PtRemappingEdge
///
/// Mostly for debugging/testing
std::ostream& operator<<(std::ostream& out,
                    const IntervalTreeRemapper::PtRemappingEdge& p) {
    const auto& e = p.s_edge;
    out << (e.source_strand() == Strand::pos ? '+' : '-') << p.id
        << "[" << p.start << "," << p.supremum << ")"
        << ">"
        << (e.target_strand() == Strand::pos ? '+' : '-') << e.target_id()
        << "@" << e.target_start();
    return out;
}

IntervalTreeRemapper::PtRemappingEdge
IntervalTreeRemapper::PtRemappingEdge::from(std::string s) {
    std::istringstream in(s);
    char s_strand_ch, t_strand_ch, _;
    IntSeqId s_id, t_id;
    uint32_t start, supremum, t_start;
    in >> s_strand_ch >> s_id >> _ >> start >> _ >> supremum >> _ >> _
       >> t_strand_ch >> t_id >> _ >> t_start;
    int s_strand = s_strand_ch == '+' ? +1 : -1;
    int t_strand = t_strand_ch == '+' ? +1 : -1;
    return PtRemappingEdge{s_id, start, supremum,
            SimplifiedEdge::from(s_strand, t_id, t_start, t_strand)};
}

inline IntervalTreeRemapper::EdgesMinLenAndPtlp
IntervalTreeRemapper::edges_for_remapping_first_point_of
(const Interval& orig_interval) const {
    EdgesMinLenAndPtlp rv;
    rv.min_len = orig_interval.length();

    if(orig_interval.length() == 0) {
        throw std::runtime_error
            ("Attempt to remap first point of empty interval: " +
             orig_interval.to_string());
    }

    std::set<IntSeqId> seen_seqids;
    std::deque<SimplifiedEdge> to_visit;
    Edge orig_as_edge{orig_interval, orig_interval};
    auto orig_as_s_edge = SimplifiedEdge::from(orig_as_edge, _seqids);
    to_visit.push_back(orig_as_s_edge);
    seen_seqids.insert(orig_as_s_edge.target_id());


    do {
        IntSeqId seqid = to_visit.front().target_id();
        auto remap_pt = to_visit.front().target_start();
        to_visit.pop_front();

        auto add_to_set = [seqid, remap_pt, &rv, &seen_seqids, & to_visit]
            (const IntervalTreeNode& overlapping) {
            if(seen_seqids.count(overlapping.value.target_id()) > 0) {
                // No need to add ... already visited this target
                return;
            }

            // Make a duplicate of the overlapping edge
            auto e = PtRemappingEdge::from(seqid, overlapping);
            // Trim parts before the start of the interval we're
            // remapping
            auto delta = remap_pt - e.start;
            e.start += delta;
            e.s_edge.inc_target_start(delta);
            // Add it to the set
            rv.edges.emplace(e);
            rv.min_len = std::min(rv.min_len, e.length());

            // Queue the target interval for a visit and ignore any
            // other intervals on this sequence.
            seen_seqids.insert(e.s_edge.target_id());
            to_visit.push_back(e.s_edge);
        };

        _mappings[seqid].visit_overlapping(remap_pt, remap_pt, add_to_set);
    } while(not to_visit.empty());
    return rv;
}


inline std::multimap<Interval, Interval>
IntervalTreeRemapper::equivalent(const Interval& orig_interval) const {
    if(orig_interval.length() == 0){
        throw std::runtime_error("Remapper called with empty interval:" +
                                 orig_interval.to_string());
    }

    // Remap each prefix that maps cleanly to a PTLP and add it and
    // all its equivalent intervals to the return value.
    std::multimap<Interval, Interval> rv;
    Interval cur_interval = orig_interval;
    Interval empty = Interval{orig_interval.id, orig_interval.supremum,
                              orig_interval.supremum, orig_interval.strand};
    while(!cur_interval.empty()){
        auto ep = equivalent_to_prefix(orig_interval);
        for(const auto& i:ep.equivalent) {
            rv.insert(std::make_pair(ep.prefix, i));
        }
        cur_interval = cur_interval.after(ep.prefix).value_or(empty);
    }

    return rv;
}
