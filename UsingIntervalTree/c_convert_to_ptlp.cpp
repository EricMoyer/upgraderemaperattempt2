#include "c_interval_tree_remapping_lib.hpp"
using namespace std;

int main(int argc, char**argv){
    /// Read the input into the lines vector
    vector<string> lines;
    {
        /// Read from first argument if given or from stdin
        istream* in = nullptr;
        unique_ptr<ifstream> fin;
        if(argc > 1){
            fin.reset(new ifstream(argv[1]));
            in = fin.get();
        } else {
            in = &cin;
        }

        string line;
        cerr << "Reading ... (" << now_str() << ")" << endl;
        while(getline(*in, line)){
            if (lines.size() % 1000000 == 0){
                cerr << "Read " << lines.size() << " lines" << endl;
            }
            lines.push_back(line);
        }
        cerr << "Done. (Read " << lines.size() << " lines)" << endl;
    }

    // Transform each line into a PackedEdge
    cerr << "Make seqid table at" << now_str() << endl;
    SeqIdTable ids;
    {
        set<SeqId> ids_set;
        for(const auto& line: lines){
            const auto& e = Edge::from_string(line);
            ids_set.insert(e.from.id);
            ids_set.insert(e.to.id);
        }
        ids.resize(ids_set.size());
        copy(ids_set.begin(), ids_set.end(), ids.begin());
    }
    cerr << "Done (" << ids.size() << " ids.) at " << now_str() << endl;

    if(DEBUG){
        cerr << "SeqIds: " << endl; //DEBUG
        for(auto id: ids){ //DEBUG
            cerr << id.to_string() << endl; //DEBUG
        } //DEBUG
        cerr << "Done" << endl; //DEBUG
    }

    cerr << "Done" << endl;

    return 0;
}
