#include "c_interval_tree_remapping_lib.hpp"
#include "catch2_test_framework.hpp" // should go last to get custom operator<<
using Catch::Matchers::Matches;
using namespace std;

TEST_CASE("now_str returns a properly formatted string", "[utils]") {
    REQUIRE_THAT(now_str(), Matches(R"(\d+-\d+-\d+T\d+:\d+:\d+)"));
}


TEST_CASE("split empty string", "[utils]") {
    REQUIRE(split("", ' ').size() == 0);
}

TEST_CASE("split normal", "[utils]") {
    REQUIRE(split("fo", 'o') == vector<string>{"f"});
    REQUIRE(split("fog", 'o') == vector<string>{"f", "g"});
    REQUIRE(split("fogo", 'o') == vector<string>{"f", "g"});
}

TEST_CASE("split adjacent delims", "[utils]") {
    REQUIRE(split("o", 'o') == vector<string>{""});
    REQUIRE(split("oo", 'o') == vector<string>{"",""});
    REQUIRE(split("foo", 'o') == vector<string>{"f",""});
    REQUIRE(split("ofo", 'o') == vector<string>{"","f"});
    REQUIRE(split("oof", 'o') == vector<string>{"","","f"});
}

TEST_CASE("join strings", "[utils]") {
    vector<string> v0{}, v1{"a"}, v2{"x", "y"}, v3{"u", "r", "l"} ;
    CHECK(join(v0.begin(), v0.end()) == "");
    CHECK(join(v0.begin(), v0.end(), "!@!@") == "");
    CHECK(join(v1.begin(), v1.end()) == "a");
    CHECK(join(v1.begin(), v1.end(), "!@!@") == "a");
    CHECK(join(v2.begin(), v2.end()) == "x, y");
    CHECK(join(v2.begin(), v2.end(), "!@!@") == "x!@!@y");
    CHECK(join(v3.begin(), v3.end()) == "u, r, l");
}

TEST_CASE("SeqId class", "[UnpackedObjects]") {
    auto check_sid = [](const SeqId& sid, string accver,
                        string acc, unsigned char ver){
        SeqId sid_copy{sid};
        REQUIRE(sid.acc == acc);
        REQUIRE(sid.ver == ver);
        REQUIRE(sid == sid);
        REQUIRE(sid == sid_copy);
        REQUIRE_FALSE(sid < sid);
        REQUIRE_FALSE(sid < sid_copy);
        REQUIRE(sid.to_string() == accver);
        std::ostringstream o; o << sid;
        REQUIRE(o.str() == accver);
    };

    SECTION("From NC") {
        auto nc = "NC_000001.10";
        auto sid_nc = SeqId::from_string(nc);
        check_sid(sid_nc, nc, "NC_000001", 10);

        SECTION("From NM") {
            auto nm = "NM_000001.10";
            auto sid_nm = SeqId::from_string(nm);
            check_sid(sid_nm, nm, "NM_000001", 10);
            REQUIRE_FALSE(sid_nm == sid_nc);
            REQUIRE_FALSE(sid_nc == sid_nm);
            REQUIRE(sid_nc < sid_nm);
            REQUIRE_FALSE(sid_nm < sid_nc);
            SECTION("From XM") {
                auto xm = "XM_000001.10";
                auto sid_xm = SeqId::from_string(xm);
                check_sid(sid_xm, xm, "XM_000001", 10);
                REQUIRE_FALSE(sid_xm == sid_nc);
                REQUIRE_FALSE(sid_nc == sid_xm);
                REQUIRE(sid_nc < sid_xm);
                REQUIRE_FALSE(sid_xm < sid_nc);

                REQUIRE_FALSE(sid_xm == sid_nm);
                REQUIRE_FALSE(sid_nm == sid_xm);
                REQUIRE(sid_nm < sid_xm);
                REQUIRE_FALSE(sid_xm < sid_nm);

                SECTION("From GenBank") {
                    auto gb = "STUFF00001";
                    auto sid_gb = SeqId::from_string(gb);
                    check_sid(sid_gb, gb, gb, 0);
                    REQUIRE_FALSE(sid_gb == sid_nc);
                    REQUIRE_FALSE(sid_nc == sid_gb);
                    REQUIRE(sid_nc < sid_gb);
                    REQUIRE_FALSE(sid_gb < sid_nc);

                    REQUIRE_FALSE(sid_gb == sid_nm);
                    REQUIRE_FALSE(sid_nm == sid_gb);
                    REQUIRE(sid_nm < sid_gb);
                    REQUIRE_FALSE(sid_gb < sid_nm);

                    REQUIRE_FALSE(sid_gb == sid_xm);
                    REQUIRE_FALSE(sid_xm == sid_gb);
                    REQUIRE(sid_xm < sid_gb);
                    REQUIRE_FALSE(sid_gb < sid_xm);
                }
            }
        }

        SECTION("From more recent NC") {
            auto nc2 = "NC_000001.11";
            auto sid_nc2 = SeqId::from_string(nc2);
            check_sid(sid_nc2, nc2, "NC_000001", 11);
            REQUIRE_FALSE(sid_nc2 == sid_nc);
            REQUIRE_FALSE(sid_nc == sid_nc2);
            REQUIRE(sid_nc2 < sid_nc);
            REQUIRE_FALSE(sid_nc < sid_nc2);
        }
    }
}

TEST_CASE("Interval class", "[UnpackedObjects]") {
    // Convenience function for creating intervals from string literals
    auto I = [](const char* s){
        return Interval::from_string(s);
    };

    string first_string{"N1.2:0:+:1"};
    const vector<Interval> sorted_intervals{
        I(first_string.c_str()),
        I("N1.1:0:+:1"),
        I("N1.1:1:+:7"),
        I("N1.1:1:+:1"),
        I("N1.1:1:-:1"),
    };

    // String functions
    CHECK(sorted_intervals[0].to_string() == first_string);
    stringstream os; os << sorted_intervals[0];
    CHECK(os.str() == first_string);

    // Length
    CHECK(sorted_intervals[0].length() == 1);
    CHECK(sorted_intervals[2].length() == 7);
    CHECK(sorted_intervals[4].length() == 1);

    // Empty
    CHECK_FALSE(sorted_intervals[0].empty());
    CHECK(I("N1.2:0:+:0").empty());

    // After
    CHECK(I("N1.1:0:+:1").after(I("N1.1:0:+:1")).value() == I("N1.1:1:+:0"));
    CHECK(I("N1.1:0:+:10").after(I("N1.1:1:+:2")).value() == I("N1.1:3:+:7"));
    CHECK_FALSE(I("N1.1:0:+:1").after(I("N1.1:0:+:2")));
    CHECK_FALSE(I("N1.1:0:+:2").after(I("N1.1:1:+:2")));


    // Comparison functions
    auto i = GENERATE(0,1,2,3,4);
    SECTION("Select second interval") {
        auto j = GENERATE(0,1,2,3,4);
        if(i < j){
            REQUIRE(sorted_intervals[i] < sorted_intervals[j]);
            REQUIRE_FALSE(sorted_intervals[i] == sorted_intervals[j]);
        }else{
            REQUIRE_FALSE(sorted_intervals[i] < sorted_intervals[j]);
            if(i == j){
                REQUIRE(sorted_intervals[i] == sorted_intervals[j]);
            }else{
                REQUIRE_FALSE(sorted_intervals[i] == sorted_intervals[j]);
            }
        }
    }
}

TEST_CASE("Edge class", "[UnpackedObjects]") {

    string first_string{"N1.2:0:+:1\tN1.1:0:+:1"};
    const vector<Edge> sorted_edges{
        Edge::from_string(first_string),
        Edge::from_string("N1.2:0:+:1\tN1.1:1:+:7"),
        Edge::from_string("N1.1:0:+:1\tN1.1:1:+:7"),
    };

    // String functions
    CHECK(sorted_edges[0].to_string() == first_string);
    stringstream os; os << sorted_edges[0];
    CHECK(os.str() == first_string);

    // Sort/swap functions
    auto rev = sorted_edges[0].reversed();
    CHECK(rev.to_string() == "N1.1:0:+:1\tN1.2:0:+:1");
    CHECK(rev.reversed().to_string() == first_string);
    CHECK(rev.sorted().to_string() == first_string);
    CHECK(sorted_edges[0].sorted().to_string() == first_string);

    // Comparison functions
    auto i = GENERATE(0,1,2);
    SECTION("Select second edge") {
        auto j = GENERATE(0,1,2);
        if(i < j){
            REQUIRE(sorted_edges[i] < sorted_edges[j]);
            REQUIRE_FALSE(sorted_edges[i] == sorted_edges[j]);
        }else{
            REQUIRE_FALSE(sorted_edges[i] < sorted_edges[j]);
            if(i == j){
                REQUIRE(sorted_edges[i] == sorted_edges[j]);
            }else{
                REQUIRE_FALSE(sorted_edges[i] == sorted_edges[j]);
            }
        }
    }
}

TEST_CASE("seqid_index_of", "[UnpackedObjects]") {

    SeqId s0{SeqId::from_string("N1.2")};
    SeqId s1{SeqId::from_string("N1.1")};
    SeqId s2{SeqId::from_string("N3.1")};
    SeqId s3{SeqId::from_string("N0.1")};
    SECTION("Empty table"){
        const SeqIdTable table{};
        REQUIRE(seqid_index_of(table, s0) == bad_idx);
    }

    SECTION("One Elt table"){
        const SeqIdTable table{s0};
        REQUIRE(seqid_index_of(table, s0) == 0);
        REQUIRE(seqid_index_of(table, s1) == bad_idx);
    }

    SECTION("Many Elt table"){
        SeqIdTable table{s0, s1, s2};
        sort(table.begin(), table.end());

        REQUIRE(seqid_index_of(table, s0) == 0);
        REQUIRE(seqid_index_of(table, s1) == 1);
        REQUIRE(seqid_index_of(table, s2) == 2);
        REQUIRE(seqid_index_of(table, s3) == bad_idx);
    }
}
