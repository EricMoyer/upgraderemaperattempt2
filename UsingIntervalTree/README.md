Overview
=======

The list-of-packed-edge representation worked poorly for preprocessing
into direct-to-ptlp alignments. Splitting of intervals created two
many intermediate interval pairs. In addition, storing the sequence id
for the source sequence is not needed and wastes space. Finally,
unpacking seems to take a lot of time (not sure about this). So, I'm
going with Brad/Eugene's solution of using an interval tree.

In one possible future, I'd implement my own without pointers (a
perfectly balanced binary tree) using it just for lookups once
created. In another future, I'll modify the tree to just point
directly to the PTLP (and mark the value as the PTLP) ... saving
lookups as I go. These are both more complex than the naiive "create a
tree and find the PTLP for each point on the sequence -- creating a
new set of alignments just with the PTLP" but one of the two may be
necessary for optimization.
