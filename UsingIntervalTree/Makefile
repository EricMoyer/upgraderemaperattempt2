CXXFLAGS=-g -march=native -fopenmp --std=c++17 --pedantic -Wall -Werror -Wextra -fmax-errors=3
LDFLAGS=$(CXXFLAGS)
CC=g++

all: c_convert_to_ptlp test_coverage/index.html

c_convert_to_ptlp: CXXFLAGS += -O3
c_convert_to_ptlp: c_convert_to_ptlp.o

c_convert_to_ptlp.o: c_convert_to_ptlp.cpp c_interval_tree_remapping_lib.hpp

test_coverage/index.html: test_coverage_filtered.info
	genhtml test_coverage_filtered.info --output-directory test_coverage

test_coverage_filtered.info: run_tests.cpp.gcov
	lcov --capture --directory . --output test_coverage.info
	lcov --remove test_coverage.info '/usr/*' '/**/catch*.hpp' '/**/submodules/**' --output test_coverage_filtered.info

run_tests.cpp.gcov: run_tests
	rm -f *.gcna
	./run_tests
	gcov test*.cpp *.hpp run_tests.cpp

run_tests: CXXFLAGS += -fprofile-arcs -ftest-coverage -O0
run_tests: run_tests.o test_c_interval_tree_remapping_lib.o
run_tests: test_IntervalTreeRemapper.o

test_IntervalTreeRemapper.o: test_IntervalTreeRemapper.cpp
test_IntervalTreeRemapper.o: IntervalTreeRemapper.hpp
test_IntervalTreeRemapper.o: submodules/intervaltree/IntervalTree.h
test_IntervalTreeRemapper.o: catch2_test_framework.hpp

run_tests.o: run_tests.cpp catch2_test_framework.hpp

test_c_interval_tree_remapping_lib.o: test_c_interval_tree_remapping_lib.cpp
test_c_interval_tree_remapping_lib.o: catch2_test_framework.hpp

clean:
	rm -f *.o c_convert_to_ptlp run_tests
	rm -rf test_coverage *.gcna *.gcno *.gcov *.info
