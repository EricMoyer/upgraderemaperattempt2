#!/usr/bin/env python3
from bisect import bisect_left
from collections import namedtuple
from functools import reduce
from itertools import chain, groupby
from operator import itemgetter, attrgetter
from sys import intern
import collections
import datetime
import fileinput
import multiprocessing.dummy as mp
import sys

Edge = namedtuple("Edge", "from_ to")
# acc: accession
# ver: positive integer version number OR -64 for no version
SeqId = namedtuple("SeqId", "acc ver")

Interval = namedtuple("Interval", "id_ first length strand")

def id_cmp_key(id_):
    return id_.acc, id_.ver

def ptlp_cmp_key_acc(acc):
    '''Prioritize different patterns and break ties with string less

    Priority Matching regex
    0 ^NC_
    1 ^N._
    2 ^.._
    3 ^.*
    '''
    if acc[2:3] == "_":
        if acc.startswith("N"):
            if acc.startswith("NC_"):
                return 0, acc
            else:
                return 1, acc
        else:
            return 2, acc
    else:
        return 3, acc

def ptlp_cmp_key(id_):
    return ptlp_cmp_key_acc(id_.acc), -id_.ver

def interval_cmp_key(interval):
    '''Sort intervals by start position and break ties so that the longest
    go first This ensures that all things interval n overlaps that
    start later will be after interval n but before things that
    interval n does not overlap.  This allows me to find overlapping
    groups by testing whether the next item overlaps anything in the
    current list.
    '''
    return (id_cmp_key(interval.id_), interval.first, -interval.length,
            interval.strand)

def edge_cmp_key(edge):
    '''Sort using interval_cmp_key() to enable finding overlaps
    see the interval_cmp_key() docstring'''
    return interval_cmp_key(edge.from_), interval_cmp_key(edge.to)

def interval_without_strand(i):
    return i.id_, i.first, i.length

def same_from_ignore_strand_key(edge):
    '''Return all position fields of from_ except for the strand'''
    return interval_without_strand(edge.from_)

def overlap_ignore_strand(i1, i2):
    return i1.id_ == i2.id_ and overlap_positions(
        i1.first, i1.first + i1.length,
        i2.first, i2.first + i2.length)

def overlap_positions(i1_first, i1_one_past_end,
                      i2_first, i2_one_past_end):
    return  (i1_first < i2_one_past_end and
             i2_first < i1_one_past_end)

def combine_overlapping_intervals(i1, i2):
    '''Return a new interval that covers both i1 and i2. They must overlap
    and be on the same strand of the same sequence'''
    new_first=min(i1.first, i2.first)
    new_last=max(i1.first + i1.length, i2.first + i2.length)
    return Interval(
        id_=i1.id_,
        strand=i1.strand,
        first=new_first,
        length=new_last - new_first)

def are_overlapping_intervals(i1, i2):
    return overlap_ignore_strand(i1, i2) and (
        i1.strand == i2.strand)

def amount_of_overlap(i1, i2):
    '''This is only valid for overlapping intervals. If they do overlap,
    it returns the number of positions they share. It is positive if i1
    starts at or before i2 and negative otherwise'''
    multiplier = 1
    if i1.first > i2.first:
        multiplier = -1
    overlap_start = max(i1.first, i2.first)
    overlap_end = min(i2.first + i2.length, i1.first + i1.length)
    return multiplier * (overlap_end - overlap_start)

def are_overlapping_edges(e1, e2):
    return are_overlapping_intervals(e1.from_, e2.from_) and (
        are_overlapping_intervals(e1.to, e2.to) and
        amount_of_overlap(e1.from_, e2.from_) ==
        amount_of_overlap(e1.to, e2.to)
    )

def combine_overlapping_edges(e1, e2):
    return Edge(from_=combine_overlapping_intervals(e1.from_, e2.from_),
                to=combine_overlapping_intervals(e1.to, e2.to))

OverlappingGroup = namedtuple("OverlappingGroup",
                              "edges containing_interval")

num_group_calls = 0
group_call_last_update = datetime.datetime.now()
def group_overlapping(cur_groups, next_item):
    '''Reducer to group a sequence into a tuple of OverlappingGroup
    objects, each of which contains all edges with first intervals
    that overlapped one another (assuming that intervals were sorted
    by start and with ties broken by longer intervals coming first)

    Initial value for reducer should be [] (must be a list because it
    is mutated for efficiency's sake)
    '''
    global num_group_calls
    global group_call_last_update
    num_group_calls += 1
    now = datetime.datetime.now()
    elapsed =  now - group_call_last_update
    if elapsed.total_seconds() > 10:
        group_call_last_update = now
        print("... still grouping: {} calls {} groups at {}".format(
            num_group_calls, len(cur_groups), now), file=sys.stderr)
    if (len(cur_groups) == 0 or
        not overlap_ignore_strand(next_item.from_,
                                  cur_groups[-1].containing_interval)):
        # If empty group list or no overlap with last, add a new
        # overlapping group with just the new item
        cur_groups.append(OverlappingGroup(
            edges=[next_item,],
            containing_interval=next_item.from_))
        return cur_groups
    else:
        # Replace the last group with one extended to contain next_item
        last, l_ivl = cur_groups[-1]
        next_ivl = next_item.from_
        new_first = min(l_ivl.first, next_ivl.first)
        new_last = max(l_ivl.first + l_ivl.length,
                       next_ivl.first + next_ivl.length)
        new_length = new_last - new_first
        last.append(next_item)
        cur_groups[-1] = OverlappingGroup(
            edges=last,
            containing_interval=Interval(
                id_=l_ivl.id_,
                first=new_first,
                length=new_length,
                strand=l_ivl.strand)
        )
        return cur_groups

seq_id_objects = {}
def seq_id_from_string(string):
    s=intern(string)
    if s not in seq_id_objects:
        sid = s.split('.')
        ver = int(sid[1]) if len(sid) > 1 else -64
        seq_id_objects[s] = SeqId(intern(sid[0]), ver)
    return seq_id_objects[s]

def seq_id_to_string(sid):
    return ("{}.{}".format(*sid)) if sid.ver >= 0 else sid.acc

def interval_from_string(s):
    n = s.split(':')
    return Interval(seq_id_from_string(n[0]), int(n[1]), int(n[3]),
                    intern(n[2]))

num_lines_processed = 0
def edge_from_line(line):
    global num_lines_processed
    if num_lines_processed % 1000000 == 0:
        sys.stderr.write('Read {} lines. Cur: {}'.format(
            num_lines_processed, line))
    num_lines_processed += 1
    return Edge(*map(interval_from_string, line.rstrip().split('\t')))

def flatten(listOfLists):
    return chain.from_iterable(listOfLists)

def interval_to_string(interval):
    return "{}:{}:{}:{}".format(
        seq_id_to_string(interval.id_), interval.first, interval.strand,
        interval.length)

def edge_to_string(edge):
    return "{}\t{}".format(*map(interval_to_string,edge))

def opposite_strand_string(strand):
    return intern('-') if strand == '+' else intern('+')

def opposite_strand(interval):
    '''Return a copy of interval on the opposite strand'''
    return Interval(
        id_=interval.id_,
        first=interval.first,
        length=interval.length,
        strand=opposite_strand_string(interval.strand))

def make_from_strand_positive(edge):
    '''Return an equivalent edge where the from strand is positive
    This inverts the strands of both edges if the from_ is on negative strand
    '''
    if edge.from_.strand == '+':
        return edge
    else:
        return Edge(*map(opposite_strand, edge))

def point_all_edges_to_ptlp(edges):
    '''Return a pair. First, a new iterable of edges expressing the same
    equivalences but where all edges point to the ptlp interval

    Second, a boolean that is true if changes are made (an edge came
        in that did not connect to the ptlp for the group that now
        does)


    Prereq: all edges in must have the same from_ inteval (except for
        strand) and all must have to intervals that are the same length
        as that from interval
    '''
    # Ensure we have at least one edge
    if len(edges) == 0:
        return edges
    # Normalize strand: make all have a from_ interval with positive
    # strand - this makes all intervals equivalent.
    edges = map(make_from_strand_positive, edges)
    # Extract the unique intervals and sort by ptlp
    # order. StackOverflow says this is faster than uniq(sorted)
    intervals = sorted(set(flatten(edges)),
                       key=lambda i: (
                           ptlp_cmp_key(i.id_), i.first, i.length, i.strand))
    ptlp = intervals[0]
    # A change will be made if some edge did not connect to the
    # ptlp sequence - this misses changes that select one out of
    # many ptlps
    def connects_to_ptlp(edge):
        return edge.from_.id_ == ptlp.id_ or edge.to.id_ == ptlp.id_
    were_changes = not all(connects_to_ptlp(e) for e in edges)
    # Turn the intervals back into edges mapping to the ptlp. I filter
    # out non-ptlp intervals rather than just skipping the first to
    # eliminate sequences aligning with themselves due to inconsistent
    # alignments.
    non_ptlp_intervals = filter(lambda i: i.id_!=ptlp.id_, intervals)
    edges = map(lambda e: Edge(from_=e, to=ptlp), non_ptlp_intervals)

    # Make all the from intervals positive-strand (this ensures that
    # we won't keep changing the strand every iteration)
    #
    # TODO: This may not be needed now that we're doing the change checking in this function
    edges = map(make_from_strand_positive, edges)
    return list(edges), were_changes

def interval_contains(interval, position):
    '''Return true if the interval contains position

    Note: treats as a half-open interval containing the first point, but not
    the last.'''
    return (position >= interval.first and
            position - interval.first < interval.length)


def offset_interval(interval, offset, length):
    '''Return a length=length Interval starting at offset after the first point in interval

    Does not check that offset and offset+length remain within the interval.
    '''
    return Interval(
        id_=interval.id_,
        first=interval.first + offset,
        length=length,
        strand=interval.strand)

num_split_edge_calls = 0
def split_with(split_points):
    def split_edge(edge):
        global num_split_edge_calls
        num_split_edge_calls += 1
        if num_split_edge_calls % 10000 == 0:
            print("... {} split edge calls at {}".format(
                num_split_edge_calls, datetime.datetime.now()),
                  file=sys.stderr)
        from_ = edge.from_
        intersecting_points = sorted(set((from_.first, *filter(
            lambda p: interval_contains(from_, p),
            split_points), from_.first + from_.length)))
        n = len(intersecting_points)
        if n < 2:
            return (edge,)
        sub_intervals = zip(
            intersecting_points[0:n-1],
            intersecting_points[1:n])
        def sub_edge(interval):
            offset = interval[0] - edge.from_.first
            length = interval[1] - interval[0]
            return Edge(
                from_=offset_interval(edge.from_, offset, length),
                to=offset_interval(edge.to, offset, length))
        return map(sub_edge, sub_intervals)
    return split_edge

def conflict_key(edge):
    '''Key for detcting conflicts'''
    return edge.from_, edge.to.id_

def conflict_resolution_key(edge):
    '''Key for sorting in conflict resolution order'''
    return conflict_key(edge), edge.to


def split_from_intervals(edges):
    '''Return pair. First, a new set of edges expressing the same
    equivalences but where none of the from_ intervals overlap

    Second, a boolean: True if any of the intervals were split.

    Prereq: all edges must have from_ intervals on the same sequence
    '''
    split_points = sorted(set(flatten(
        (e.from_.first, e.from_.first + e.from_.length)
        for e in edges)))
    split_edges = tuple(flatten(map(split_with(split_points), edges)))
    changes_were_made = sorted(split_edges) != sorted(edges)
    return split_edges, changes_were_made


        # ############## TODO remove this old code #################

        # # pass_back_changes_were_made[0] = (
        # #     pass_back_changes_were_made[0] or
        # #     sorted(split_edges) != sorted(edges))
        # same_from = groupby(split_edges, key=lambda e:e.from_)
        # new_edges = flatten(
        #     point_all_edges_to_ptlp(list(group[1]), pass_back_changes_were_made)
        #     for group in same_from)

        # if False:  # or type(next(iter(new_edges)) == "tuple"):
        #     print("Producing new edges from:", file=sys.stderr)
        #     print("\n".join(map(str, edges)), file=sys.stderr)
        #     print("Split:", file=sys.stderr)
        #     print("\n".join(map(str, split_edges)), file=sys.stderr)
        #     print("Same from:", file=sys.stderr)
        #     print("\n".join(map(str, same_from)), file=sys.stderr)
        #     print("New edges:", file=sys.stderr)
        #     print("\n".join(map(str, new_edges)), file=sys.stderr)
        #     print("Done\n", file=sys.stderr)

        # return new_edges

# From recipes in docs
def all_equal(iterable):
    g = groupby(iterable)
    return next(g, True) and not next(g, False)

def contains_identical_from_intervals(edges):
    def no_strand(interval):
        return interval.id_, interval.first, interval.length
    return all_equal(map(lambda e:no_strand(e.from_), edges))

def are_ordered_adjacent_intervals(i1, i2):
    '''Return true if i1 is before i2 on the same strand of the same sequence and
    there is no space between the end of i1 and the start of i2'''
    return (i1.id_ == i2.id_ and
            i1.strand == i2.strand and
            i1.first + i1.length == i2.first)


def are_ordered_adjacent_edges(edge1, edge2):
    '''Return true if the from and to intervals are adjacent'''
    return (are_ordered_adjacent_intervals(edge1.from_, edge2.from_) and
            are_ordered_adjacent_intervals(edge1.to, edge2.to))


def combine_ordered_adjacent_intervals(i1, i2):
    '''Return smallest interval containing i1 and i2 on the same strand

    Prereq: are_ordered_adjacent_intervals(i1, i2) is true'''
    return Interval(
        id_=i1.id_,
        strand=i1.strand,
        first=i1.first,
        length=i1.length + i2.length)

def combine_ordered_adjacent_edges(e1, e2):
    '''Return an edge whose from interval combines the from intervals of e1 and e2
    and whose to interval combines their to intervals.

    Prereq: are_ordered_adjacent_edges(e1, e2)'''
    return Edge(
        from_=combine_ordered_adjacent_intervals(e1.from_, e2.from_),
        to=combine_ordered_adjacent_intervals(e1.to, e2.to))

def remove_duplicates(iterable):
    return (key for key,_ in groupby(iterable))

def reverse_edge(edge):
    return Edge(from_=edge.to, to=edge.from_)

def add_reverse_edges(edge):
    return edge, reverse_edge(edge)

def add_self_edges(edge):
    return edge, Edge(edge.to, edge.to), Edge(edge.from_, edge.from_)

# Edge: the edge in the cluster
#
# from_point: the position on the from sequence that was used to
#     generate this cluster
#
# to_point: the point on the to sequence that corresponds to from_point
#
# parent_idx: the index in the cluster of the parent edge that
#     generated this edge
#
# A cluster is a non-empty list of ClusterEdge objects returned by
# edge_cluster_for(interval).
#
# * All the parent_idx fields are accurate
#
# * All edges with parent_idx == -1 have a from_ interval on the same
#   sequence
#
# * There are no loops in the graph made by the edges even when
#   considered to connect only seq-ids
ClusterEdge=namedtuple("ClusterEdge", "edge from_point to_point parent_idx")


def cluster_edge_for(edge, from_point, parent_idx):
    '''Return a cluster edge with from and to-points set based on from_point
    Note: edge.from_.length must equal edge.to.length
    '''
    return ClusterEdge(
        edge=edge,
        from_point=from_point,
        to_point=from_point + edge.to.first - edge.from_.first,
        parent_idx=parent_idx)

def seq_ids_in_edges(cluster_edges):
    return map(attrgetter('id_'), flatten(cluster_edges))

def interval_first_point(interval):
    # The first point contained in the interval ... not valid for
    # empty intervals.
    return interval.id_, interval.first if interval.length > 0 else None

def interval_last_point(interval):
    # The last point contained in the interval ... not valid for empty
    # intervals
    return (interval.id_, interval.first + interval.length - 1
            if interval.length > 0 else None)

def path_to_ptlp(cluster):
    '''Given a cluster (see comment on ClusterEdge above), return
    ptlp_path, ptlp_id

    ptlp_path is a list of ClusterEdge objects in the order they are traversed
        to get to the ptlp from the initial sequence.

    ptlp_id is the SeqId of the Preferred Top Level Placement (PTLP) sequence
        out of all the sequences in the cluster.

    '''
    ptlp_id = min(set(interval.id_ for interval in
                   flatten(ce.edge for ce in cluster)), key=ptlp_cmp_key)
    ptlp_edges = (ce for ce in cluster if ce.edge.from_.id_ == ptlp_id or
                  ce.edge.to.id_ == ptlp_id)
    def path_from_progenitor(cluster_edge):
        if cluster_edge.parent_idx == -1:
            return [cluster_edge]
        else:
            p = path_from_progenitor(cluster[cluster_edge.parent_idx])
            p.append(cluster_edge)
            return p
    def index_of_ptlp(cluster_edge):
        if cluster_edge.edge.from_.id_ == ptlp_id:
            return 0
        elif cluster_edge.edge.to.id_ == ptlp_id:
            return 1
        else:
            return None

    paths = map(path_from_progenitor, ptlp_edges)
    ptlp_path = min(paths, key=lambda p: (len(p), index_of_ptlp(p[-1])))
    # Detect if the edge starts on the ptlp sequence ... and rewrite
    # it so it maps to itself.
    if len(ptlp_path) == 1 and ptlp_path[0].edge.from_.id_ == ptlp_id:
        old_edge = ptlp_path[0]
        ptlp_interval = old_edge.edge.from_
        ptlp_path = [
            ClusterEdge(
                edge=Edge(from_=ptlp_interval, to=ptlp_interval),
                from_point=old_edge.from_point,
                to_point=old_edge.from_point,
                parent_idx=-1)]

    return ptlp_path, ptlp_id


def bounds_and_offset(ptlp_path, interval):
    '''Return new_bounds, offset, ptlp_strand

    new_bounds: a pair of points giving the maximum and minimum values
        in interval that align with the ptlp using the edges in ptlp_path
        in order

    offset: the number to add to a position in interval to get a
        corresponding position on the ptlp sequence

    ptlp_strand: the strand a point in interval will be on in the ptlp sequence

    '''
    new_bounds = interval.first, interval.first + interval.length
    offset = 0
    ptlp_strand = interval.strand

    if interval.id_ != ptlp_path[0].edge.from_.id_:
        raise ValueError('The interval to start computing bounds should be '
                         'on the same sequence as the start of the path to '
                         'the ptlp')

    for e in ptlp_path:
        # Subtracting offset converts to coordinates back to interval
        # coordinates. The current lower bound will get you to the
        # "to" interval of the previous edge. The from interval of
        # this edge is the same sequence (so has no new offset) but
        # possibly different bounds. The lower bound can't decrease
        # and so is the maximum of the current lower bound and the
        # first point in the from_ interval when translated to
        # original interval coordinates. Similarly for the upper
        # bound, which can't increase.
        lower_bound = max(new_bounds[0], e.edge.from_.first - offset)
        upper_bound = min(new_bounds[1],
                          e.edge.from_.first + e.edge.from_.length - offset)
        new_bounds = lower_bound, upper_bound;

        # Here we update parameters to the "to" strand. Bounds stay the same
        # because the two aligned intervals are the same length
        offset = offset + e.edge.to.first - e.edge.from_.first
        if e.edge.from_.strand != e.edge.to.strand:
            ptlp_strand = opposite_strand_string(ptlp_strand)

    return new_bounds, offset, ptlp_strand



if __name__== '__main__':
    edges = (edge_from_line(line) for line in fileinput.input())
    sys.stderr.write('Reading and sorting ...\n')
    # No zero-length intervals
    edges = filter(lambda e: e.from_.length != 0 and e.to.length != 0, edges)
    edges = remove_duplicates(sorted(
        flatten(map(add_reverse_edges, edges)),
        key=edge_cmp_key))
    sys.stderr.write('Done\n')

    # Split based on overlapping from_ so that all from_ intervals are
    # disjoint unless they are equal
    sys.stderr.write('Starting split edges at {}...'.format(
        datetime.datetime.now()))
    edges = map(attrgetter("edges"),
                reduce(group_overlapping, edges, []))
    with mp.Pool() as pool:
        edges = pool.map(split_from_intervals, edges)
    edges = flatten(map(itemgetter(0), edges))
    edges = list(remove_duplicates(sorted(edges, key=edge_cmp_key)))
    sys.stderr.write('Done\n')
    sys.stderr.flush()


    # Just the key on which bisect will operate to find the edges containing
    # the point.
    edge_firsts = [interval_first_point(e.from_) for e in edges]

    def cluster_edges_containing_point(point, parent_idx):
        '''Return a list of ClusterEdges whose edges contained images of point.

        The returned value is a list, so it can be traversed multiple
        times and its edges are sorted for reproducibility
        '''
        cluster_edges = []
        point_first = interval_first_point(point)
        # Calculate the list edges with overlapping from
        # intervals. The list will be edges[start_idx:idx_supremum]

        # Start the supremum at the first interval which starts at or
        # after the point.
        idx_supremum = bisect_left(edge_firsts, point_first)
        start_idx = idx_supremum
        # Move the supremum to be after all intervals which start at
        # the search point. The ordering guarantees that no later
        # intervals can contain this point. And the lack of empty
        # intervals guarantees that all of those intervals do contain
        # this point.
        while idx_supremum < len(edge_firsts) and (
                edge_firsts[idx_supremum] == point_first):
            idx_supremum += 1

        # if the interval is non-empty, we've found all the
        # overlapping edges (since the from_ intervals of the edges
        # are disjoint or identical)
        if start_idx != idx_supremum:
            return [cluster_edge_for(e, point.first, parent_idx)
                    for e in edges[start_idx:idx_supremum]]

        # The interval at the supremum did not contain the point. So,
        # we check the interval before it.
        if start_idx == 0:
            return []
        if not overlap_ignore_strand(edges[start_idx-1].from_, point):
            # If the preceding interval does not contain the point, we're in
            # a gap. No matches
            return []
        # It did contain the point and all intervals are either
        # disjoint or identical, so we can set the start to the last
        # interval that matches the one we just found.
        start_idx -= 1
        containing_from = edges[start_idx].from_

        while start_idx > 0 and edges[start_idx-1].from_ == containing_from:
            start_idx -= 1

        return [cluster_edge_for(e, point.first, parent_idx)
                for e in edges[start_idx:idx_supremum]]


    def edge_cluster_for(interval):
        '''Return the edges from the alignment for the first point of interval
        The returned cluster will not be empty. If there are no edges
        in the alignment, the sequence at that point will be treated as if
        it is its own ptlp.

        The search is breadth-first so the back-trace path in the
        cluster is the shortest path to that edge.
        '''
        point = Interval(
            id_=interval.id_,
            first=interval.first,
            length=1,
            strand=interval.strand)

        waiting_edges=collections.deque()
        seen_seq_ids=set()
        def add_unseen_edges(new_edges):
            '''Add edges that don't have a previously seen sequence as "to"
            This is a loop rather than filter because I want previous items
            in the list to block later ones.'''
            for ce in new_edges:
                if ce.edge.to.id_ not in seen_seq_ids:
                    waiting_edges.appendleft(ce)
                    seen_seq_ids.add(ce.edge.from_.id_)
                    seen_seq_ids.add(ce.edge.to.id_)

        add_unseen_edges(cluster_edges_containing_point(point, -1))
        cluster_edges = []
        while len(waiting_edges):
            to_explore = waiting_edges.pop()
            cluster_edges.append(to_explore)
            # Get edges containing the image of the original point that
            # connect to unseen seq_ids
            to_explore_pt = Interval(
                id_=to_explore.edge.to.id_,
                first=to_explore.to_point,
                length=1,
                strand=to_explore.edge.to.strand  # strand should be ignored
            )
            add_unseen_edges(
                cluster_edges_containing_point(
                to_explore_pt, len(cluster_edges)-1))

        if len(cluster_edges) == 0:
            # No alignments of first point ... treat it as a PTLP
            cluster_edges=[cluster_edge_for(
                edge=Edge(from_=point, to=point),
                from_point=point.first,
                to_point=point.first,
                parent_idx=-1)]

        return cluster_edges

    intervals = sorted(set(flatten(edges)), key=interval_cmp_key)
    num_intervals_mapped = 0
    def ptlp_edges(interval):
        global num_intervals_mapped
        if num_intervals_mapped % 100 == 0:
            print("... {} intervals of {} mapped to ptlp at {}".format(
                num_intervals_mapped, len(intervals), datetime.datetime.now()),
                  file=sys.stderr)
        num_intervals_mapped += 1
        new_edges = []
        while interval.length > 0:
            cluster = edge_cluster_for(interval)
            ptlp_path, ptlp_id = path_to_ptlp(cluster)
            new_bounds, offset, ptlp_strand = bounds_and_offset(
                ptlp_path, interval)

            # Append the a new edge based on the bounds to the list of edges
            new_from = Interval(
                id_=interval.id_,
                first=new_bounds[0],
                length=new_bounds[1] - new_bounds[0],
                strand=interval.strand)
            new_to = Interval(
                id_=ptlp_id,
                first=new_from.first + offset,
                length=new_from.length,
                strand=ptlp_strand)
            new_edges.append(Edge(from_=new_from, to=new_to))

            # Chop off the part of the interval that got used
            # new_bounds can never be longer than the original bounds
            interval = Interval(
                id_=interval.id_,
                first=new_bounds[1],
                length=interval.length - new_from.length,
                strand=interval.strand)
        return new_edges

    # This code to coalesce adjacent overlapping intervals is a
    # repeat of the code below that coalesces adjacent overlapping
    # edges for printing. It should be abstracted away.
    coalesced_intervals = []
    cur_interval = {intern('+'): None, intern('-'): None }
    while len(intervals) > 0:
        i = intervals.pop()
        if cur_interval[i.strand] is None:
            cur_interval[i.strand] = i
        elif are_overlapping_intervals(cur_interval[i.strand], i):
            cur_interval[i.strand] = combine_overlapping_intervals(
                cur_interval[i.strand], i)
        elif are_ordered_adjacent_intervals(cur_interval[i.strand], i):
            cur_interval[i.strand] = combine_ordered_adjacent_intervals(
                cur_interval[i.strand], i)
        else:
            coalesced_intervals.append(cur_interval[i.strand])
            cur_interval[i.strand] = i
    if cur_interval['+'] is not None:
        coalesced_intervals.append(cur_interval['+'])
    if cur_interval['-'] is not None:
        coalesced_intervals.append(cur_interval['-'])
    intervals = coalesced_intervals
    coalesced_intervals = None
    with mp.Pool() as pool:
        edges = list(sorted(set(filter(
            lambda e:e.from_ != e.to, # remove self edges
            flatten(pool.map(ptlp_edges, intervals)))),
            key=edge_cmp_key))

    # Print edges combining ordered adjacent edges
    waiting_to_print = None
    for e in edges:
        if waiting_to_print is None:
            waiting_to_print = e
        elif are_overlapping_edges(waiting_to_print, e):
            waiting_to_print = combine_overlapping_edges(
                waiting_to_print, e)
        elif are_ordered_adjacent_edges(waiting_to_print, e):
            waiting_to_print = combine_ordered_adjacent_edges(
                waiting_to_print, e)
        else:
            print(edge_to_string(waiting_to_print))
            waiting_to_print = e
    if waiting_to_print is not None:
        print(edge_to_string(waiting_to_print))
        waiting_to_print = None
