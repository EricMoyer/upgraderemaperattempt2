#include "c_packed_edge_remapping_lib.hpp"

int main(int argc, char**argv){
    /// Read the input into the lines vector
    vector<string> lines;
    {
        /// Read from first argument if given or from stdin
        istream* in = nullptr;
        unique_ptr<ifstream> fin;
        if(argc > 1){
            fin.reset(new ifstream(argv[1]));
            in = fin.get();
        } else {
            in = &cin;
        }

        string line;
        cerr << "Reading ... (" << now_str() << ")" << endl;
        while(getline(*in, line)){
            if (lines.size() % 1000000 == 0){
                cerr << "Read " << lines.size() << " lines" << endl;
            }
            lines.push_back(line);
        }
        cerr << "Done. (Read " << lines.size() << " lines)" << endl;
    }

    // Transform each line into a PackedEdge
    cerr << "Make seqid table at" << now_str() << endl;
    SeqIdTable ids;
    {
        set<SeqId> ids_set;
        for(const auto& line: lines){
            const auto& e = Edge::from_string(line);
            ids_set.insert(e.from.id);
            ids_set.insert(e.to.id);
        }
        ids.resize(ids_set.size());
        copy(ids_set.begin(), ids_set.end(), ids.begin());
    }
    cerr << "Done (" << ids.size() << " ids.) at " << now_str() << endl;

    if(DEBUG){
        cerr << "SeqIds: " << endl; //DEBUG
        for(auto id: ids){ //DEBUG
            cerr << id.to_string() << endl; //DEBUG
        } //DEBUG
        cerr << "Done" << endl; //DEBUG
    }

    cerr << "Make packed edge objects and their reverse at "
         << now_str() << endl;
    vector<PackedEdge> packed_edges(2*lines.size());
    {
        par::transform
            (lines.begin(), lines.end(), packed_edges.begin(),
             [&ids](const auto& line){
                return Edge::from_string(line).pack(ids); });
        par::transform
            (packed_edges.begin(), packed_edges.begin() + lines.size(),
             packed_edges.begin() + lines.size(),
             [](const auto& edge){ return edge.reverse(); });
        lines.resize(0); // Don't need the lines anymore, free up the memory.
        par::sort(packed_edges.begin(), packed_edges.end());
        auto new_end = unique(packed_edges.begin(), packed_edges.end());
        packed_edges.erase(new_end, packed_edges.end());
    }
    cerr << "Done (" << packed_edges.size() << " unique edges.) "
         << now_str() << endl;

    // Split edges so that "from" intervals are either disjoint or identical
    split_overlapping_edges(packed_edges);

    if(DEBUG){
        cerr << "Split edges:" << endl;
        for(auto e: packed_edges){
            cerr << "Edge:" << e << endl;
        }
        cerr << "Done." << endl;
    }

    vector<PackedInterval> to_remap(2*packed_edges.size());

    {
        cerr << "Generating raw intervals to remap: " << now_str() << endl;
        auto to_remap_back1 = to_remap.begin();
        auto to_remap_back2 =
            par::transform(packed_edges.begin(), packed_edges.end(),
                           to_remap_back1,
                           [](const PackedEdge e) { return e.packed_from(); });
        cerr << (to_remap_back2-to_remap.begin()) << " intervals from "
             << " first transformation." << endl;
        auto to_remap_back4 =
            par::transform(packed_edges.begin(), packed_edges.end(),
                           to_remap_back2,
                           [](const PackedEdge e) { return e.packed_to(); });
        if(DEBUG){
            cerr << "to_remap before unique:" << endl;
            for(auto i: to_remap){
                cerr << "Interval:" << i << endl;
            }
            cerr << "Done." << endl;
        }
        cerr << (to_remap_back4-to_remap.begin()) << " intervals from "
             << " first+second transformation." << endl;
        par::sort(to_remap.begin(), to_remap_back4);
        auto to_remap_back5 =
            unique(to_remap.begin(), to_remap_back4);
        cerr << (to_remap_back5-to_remap.begin()) << " unique intervals from "
             << " second transformation." << endl;
        to_remap.erase(to_remap_back5, to_remap.end());
        cerr << "Done (" << to_remap.size() << " intervals)"
             << now_str() << endl;

        if(DEBUG){
            cerr << "Printing raw intervals ... " << endl; //DEBUG
            for(auto i: to_remap){ //DEBUG
                cerr << "Interval: " << i << endl; //DEBUG
            } //DEBUG
            cerr << "Done." << endl;
        }

        cerr << "Coallescing adjacent intervals: " << now_str() << endl;
        auto to_remap_new_end = to_remap.begin();
        unique_ptr<PackedInterval> cur_interval[2];
        for(auto it = to_remap.begin(); it != to_remap.end(); ++it){
            auto i = *it;
            unique_ptr<PackedInterval>& cur = cur_interval[i.is_pos()];
            if (not cur) {
                cur.reset(new PackedInterval(i));
            } else if(cur->overlaps_or_adjacent(i)) {
                *cur = cur->plus_overlapping_or_adjacent(i);
            } else {
                // No overlap
                *to_remap_new_end++ = *cur;
                *cur = i;
            }
        }
        if(cur_interval[0]){
            *to_remap_new_end++ = *cur_interval[0];
        }
        if(cur_interval[1]){
            *to_remap_new_end++ = *cur_interval[1];
        }
        cerr << "Done (" << to_remap.size() << " intervals to remap.)" << endl;
    }

    cerr << "Remapping intervals: " << now_str() << endl;
    const auto& const_packed_edges = packed_edges;
    vector<vector<PackedEdge>> remappings(to_remap.size());
    par::transform
        (to_remap.begin(), to_remap.end(), remappings.begin(),
         [&const_packed_edges](const PackedInterval& i) {
            return ptlp_edges(i, const_packed_edges);
        });
    to_remap.resize(0); // Not needed anymore
    cerr << "Done" << endl;


    vector<PackedEdge>& edges_from_remapping = packed_edges;
    packed_edges.clear();
    cerr << "Removing self-edges & flattening: " << now_str() << endl;
    while(not remappings.empty()){
        const auto& remapping = remappings.back();
        for(const PackedEdge e: remapping) {
            if (e.from_seqid_idx() != e.to_seqid_idx()){
                edges_from_remapping.push_back(e);
            }
        }
        remappings.pop_back();
    }
    remappings.resize(0); // Not needed now
    par::sort(packed_edges.begin(), packed_edges.end());
    auto new_end = unique(packed_edges.begin(), packed_edges.end());
    packed_edges.erase(new_end, packed_edges.end());
    cerr << "Done (" << edges_from_remapping.size()
         << " non-self-edges.)" << endl;


    cerr << "Writing out results: " << now_str() << endl;
    unique_ptr<PackedEdge> waiting_to_print;
    for(const auto& e: edges_from_remapping){
        if(not waiting_to_print) {
            waiting_to_print.reset(new PackedEdge(e));
        } else if(waiting_to_print->overlaps_or_adjacent(e)) {
            *waiting_to_print =
                waiting_to_print->plus_overlapping_or_adjacent(e);
        } else {
            cout << waiting_to_print->unpack(ids).to_string() << endl;
            *waiting_to_print = e;
        }
    }
    if(waiting_to_print) {
        cout << waiting_to_print->unpack(ids).to_string() << endl;
        waiting_to_print.release();
    }
    cerr << "Done" << endl;

    return 0;
}
