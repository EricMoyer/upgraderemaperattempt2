#pragma once
#include <atomic>
#include <algorithm>
#include <bitset>
#include <cstdint>
#include <ctime>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <parallel/algorithm>
#include <set>
#include <sstream>
#include <string>
#include <tuple>

using namespace std; // BAD BAD BAD...
namespace par = __gnu_parallel; // Bad (but not quite as bad)

constexpr bool DEBUG{false};

/// Current local time as a string
string now_str() {
    auto t = time(nullptr);
    auto tm = localtime(&t);
    ostringstream s;
    s << put_time(tm, "%Y-%m-%dT%H:%M:%S");
    return s.str();
}

/// String split from https://stackoverflow.com/a/236803/309334
template<typename Out>
void split(const std::string &s, char delim, Out result) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

/// String split from https://stackoverflow.com/a/236803/309334
std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
}

struct SeqId {
    /// The accession part of the seq id
    string acc;
    /// Version ... 0 if absent
    unsigned char ver;

    static SeqId from_string(const string& s){
        auto fields = split(s, '.');
        if (fields.size() > 1){
            return SeqId{fields.at(0), static_cast<unsigned char>(stoul(fields.at(1)))};
        }else{
            return SeqId{fields.at(0), 0};
        }
    }

    /// The extra priority given to the accession because of its special form
    ///
    /// Accessions matching regexes get the given priority
    /// 0 ^NC_
    /// 1 ^N._
    /// 2 ^.._
    /// 3 ^.*
    int priority() const{
        if (acc.size() >= 3 and acc[2] == '_'){
            if (acc.size() >= 1 and acc[0] == 'N') {
                if(acc.size() >= 2 and acc[1] == 'C') {
                    return 0;
                } else {
                    return 1;
                }
            } else {
                return 2;
            }
        } else {
            return 3;
        }
    }

    /// This is an approximate PTLP ordering
    ///
    /// Sort as tuple of priority, accession and version except:
    /// version is reversed (so larger versions of the same sequence go first).
    bool operator<(const SeqId& s) const {
        auto p = priority(), sp = s.priority();
        return tie(p, acc, s.ver) < tie(sp, s.acc, ver);
    }

    bool operator==(const SeqId& s) const {
        return tie(acc, s.ver) == tie(s.acc, ver);
    }


    string to_string() const {
        int v = ver;
        if(v > 0){
            return acc + "." + std::to_string(static_cast<int>(ver));
        }else{
            return acc;
        }
    }
};

/// Sorted list of SeqIds
///
/// Must be in PTLP order
using SeqIdTable = vector<SeqId>;

struct PackedInterval;

struct Interval {
    /// The seq id for the interval
    SeqId id;

    /// First position possibly in the interval.
    uint32_t start;

    /// First position after the interval (= start for empty interval)
    /// Guaranteed >= start
    uint32_t supremum;

    /// '+' for positive strand, '-' for negative strand
    char strand;

    /// Number of positions in the interval
    uint32_t length() const{
        return supremum - start;
    }

    static Interval from_string(const string& s){
        auto fields = split(s, ':');
        uint32_t start = stoul(fields.at(1));
        uint32_t len = stoul(fields.at(3));
        return Interval{
            SeqId::from_string(fields.at(0)),
            start, start+len, fields.at(2)[0]};
    }

    /// Compare as a tuple
    /// Note that supremum is reverse-ordered so longer intervals go first
    bool operator<(const Interval& i) const {
        return tie(id, start, i.supremum, strand) <
            tie(i.id, i.start, supremum, i.strand);
    }

    bool operator==(const Interval& i) const {
        return tie(id, start, i.supremum, strand) ==
            tie(i.id, i.start, supremum, i.strand);
    }

    string to_string() const {
        return id.to_string() + ":" + std::to_string(start) + ":" +
            strand + ":" + std::to_string(length());
    }

    PackedInterval pack(const SeqIdTable&) const;
};

constexpr uint32_t bad_idx = numeric_limits<uint32_t>::max();

/// Return the index of the SeqId in the table
///
/// returns bad_idx if the value is not found
///
/// This will actually fit in 19 bits ...
uint32_t seqid_index_of(const SeqIdTable& t, const SeqId& id){
    auto it = lower_bound(t.begin(), t.end(), id);
    if (it == t.end() or !(*it == id)){
        return bad_idx;
    } else {
        return it - t.begin();
    }
}

struct PackedEdge;

struct Edge {
    Interval from;
    Interval to;

    static Edge from_string(const string& s) {
        const auto& fields = split(s, '\t');
        return Edge{
            Interval::from_string(fields.at(0)),
            Interval::from_string(fields.at(1))};
    }

    bool operator<(const Edge&e) const {
        return tie(from, to) < tie(e.from, e.to);
    }

    bool operator==(const Edge&e) const {
        return tie(from, to) == tie(e.from, e.to);
    }

    /// Assume that ids contains the SeqIds in from and in to
    PackedEdge pack(const SeqIdTable& ids) const;

    string to_string() const {
        return from.to_string() + "\t" + to.to_string();
    }
};

constexpr size_t bits_in_packed_edge = 94;
using PackedEdgeBits = bitset<bits_in_packed_edge>;

namespace PackedEdgeConstants {
    /// 19 bits of 1s
    constexpr PackedEdgeBits ones19(0x7FFFFULL);
    constexpr unsigned long long ones19_ull{0x7FFFFULL};

    /// 18 bits of 1s
    constexpr PackedEdgeBits ones18(0x3FFFFULL);
    constexpr unsigned long long ones18_ull{0x3FFFFULL};

    /// 64 bits of 1s
    constexpr PackedEdgeBits ones64(0xFFFFFFFFFFFFFFFFULL);

    // all bits of a ulong as 1s
    constexpr PackedEdgeBits ones_ulong(numeric_limits<unsigned long>::max());

    // 1 bit of 1s
    constexpr PackedEdgeBits ones1(0x1);

    // Maximum possible length
    constexpr unsigned long max_length = (1 << 18) - 1;

    // Amount to shift so that the given value has its low bit at 0
    constexpr auto from_seqid_shift = (bits_in_packed_edge-19);
    constexpr auto from_start_shift = (bits_in_packed_edge-19-18);
    constexpr auto length_shift = (bits_in_packed_edge-19-18-18);
    constexpr auto from_strand_shift = (bits_in_packed_edge-19-18-18-1);
    constexpr auto to_seqid_shift = (bits_in_packed_edge-19-18-18-1-19);
    constexpr auto to_start_shift = (bits_in_packed_edge-19-18-18-1-19-18);
    constexpr auto to_strand_shift = (bits_in_packed_edge-19-18-18-1-19-18-1);
}

constexpr size_t bits_in_packed_interval = 56;
struct PackedInterval {
    static_assert(numeric_limits<unsigned long long>::digits==64,
                  "Need different type for PackedInterval ... current assumes that "
                  "bitset::to_ullong returned 64 bit integer.");

    /// Packed interval has the same layout as the first
    /// bits_in_packed_interval bits of the PackedEdge
    uint64_t bits;

    uint32_t seqid_idx() const {
        using namespace PackedEdgeConstants;
        return (bits >> reshift(from_seqid_shift)) & ones19_ull;
    }

    uint32_t start() const {
        using namespace PackedEdgeConstants;
        return (bits >> reshift(from_start_shift)) & ones18_ull;
    }

    uint32_t length() const {
        using namespace PackedEdgeConstants;
        return max_length - ((bits >> reshift(length_shift)) & ones18_ull);
    }

    bool is_pos() const {
        using namespace PackedEdgeConstants;
        return (bits >> reshift(from_strand_shift)) & 1;
    }

    uint32_t supremum() const {
        return start() + length();
    }

    bool contains(const uint32_t position) const {
        return position >= start() and
            position - start() < length();
    }

    bool overlaps_ignoring_strand(PackedInterval i) const {
        return seqid_idx() == i.seqid_idx() and
            start() < i.supremum() and
            i.start() < supremum();
    }

    bool overlaps_or_adjacent(PackedInterval i) const {
        return seqid_idx() == i.seqid_idx() and
            start() <= i.supremum() and
            i.start() <= supremum() and is_pos() == i.is_pos();
    }


    /// Return the smallest interval containing *this and i
    ///
    /// Prereq: this->overlaps_or_adjacent(i)
    PackedInterval plus_overlapping_or_adjacent(PackedInterval i) const {
        auto new_start = min(start(), i.start());
        auto new_supremum = max(supremum(), i.supremum());
        return PackedInterval::from
            (seqid_idx(), new_start, new_supremum - new_start , is_pos());
    }

    /// This is only valid for overlapping intervals. If they do
    /// overlap, it returns the number of positions they share. It is
    /// positive if *this starts at or before i and negative
    /// otherwise'''
    int64_t amount_of_overlap(PackedInterval i) const {
        int multiplier = start() > i.start()?-1:1;
        int64_t overlap_start{max(start(), i.start())};
        int64_t overlap_supremum{max(supremum(), i.supremum())};
        return multiplier * (overlap_supremum - overlap_start);
    }

    Interval unpack(const SeqIdTable& ids) const {
        return Interval{ids[seqid_idx()], start(), start() + length(),
                      is_pos()?'+':'-'};
    }


    static PackedInterval from(uint32_t seqid_idx, uint32_t start, uint32_t length, bool is_pos){
        using namespace PackedEdgeConstants;
        return PackedInterval
            {((max_length - length) & ones18_ull) << reshift(length_shift) |
            (seqid_idx & ones19_ull) << reshift(from_seqid_shift) |
            (start & ones18_ull) << reshift(from_start_shift) |
            (is_pos & 1) << reshift(from_strand_shift)};
    }

    bool operator<(PackedInterval i) const {
        return bits < i.bits;
    }

    bool operator==(PackedInterval i) const {
        return bits == i.bits;
    }

private:
    /// Turn shifts in the packed edge into packed interval shifts
    static constexpr size_t reshift(size_t packed_edge_shift) {
        return packed_edge_shift + bits_in_packed_interval - bits_in_packed_edge;
    }
};

ostream& operator<<(ostream& out, PackedInterval i){
    return out << i.seqid_idx() << ":" << i.start() << ":"
               << (i.is_pos()?'+':'-') << i.length();
}

struct PackedEdge {
    /// This is 94 bits in total.
    ///
    /// This treats all sequences as the same length. We could pack them
    /// much better if shorter sequences took different lengths.
    ///
    /// Layout:
    /// 75-93 - 19 bit From-sequence seq-id: index in table
    ///
    /// 57-74 - 18 bit start position (good only for human)
    ///
    /// 39-56 - 18 bit length - stored subtracted from its max
    ///               so sorting puts longer sequences first
    ///
    /// 38-38 - 1 bit from_strand: 0 if negative 1 if positive
    ///
    /// 19-37 - 19 bit To-sequence seq-id: index in table
    ///
    /// 01-18 - 18 bit start position
    ///
    /// 00-00 - 1 bit to_strand: 0 if negative 1 if positive
    PackedEdgeBits bits;

    PackedInterval packed_from() const {
        using namespace PackedEdgeConstants;
        return PackedInterval{(bits >> from_strand_shift).to_ullong()};
    }

    PackedInterval packed_to() const {
        using namespace PackedEdgeConstants;
        return PackedInterval::from(to_seqid_idx(), to_start(), length(),
                                    to_is_pos());
    }

    uint32_t from_seqid_idx() const {
        using namespace PackedEdgeConstants;
        return ((bits >> from_seqid_shift) & ones19).to_ulong();
    }

    uint32_t from_start() const {
        using namespace PackedEdgeConstants;
        return ((bits >> from_start_shift) & ones18).to_ulong();
    }

    uint32_t length() const {
        using namespace PackedEdgeConstants;
        return max_length - ((bits >> length_shift) & ones18).to_ulong();
    }

    bool from_is_pos() const {
        using namespace PackedEdgeConstants;
        return ((bits >> from_strand_shift) & ones1).to_ulong();
    }

    uint32_t to_seqid_idx() const {
        using namespace PackedEdgeConstants;
        return ((bits >> to_seqid_shift) & ones19).to_ulong();
    }

    uint32_t to_start() const {
        using namespace PackedEdgeConstants;
        return ((bits >> to_start_shift) & ones18).to_ulong();
    }

    bool to_is_pos() const {
        using namespace PackedEdgeConstants;
        return ((bits >> to_strand_shift) & ones1).to_ulong();
    }

    Edge unpack(const SeqIdTable& ids) const {
        using namespace PackedEdgeConstants;

        return Edge
            {Interval{ids[from_seqid_idx()], from_start(), from_start() + length(),
                      from_is_pos()?'+':'-'},
            Interval{ids[to_seqid_idx()], to_start(), to_start() + length(),
                      to_is_pos()?'+':'-'}};
    }

    /// Return true if the two edges overlap or are adjacent
    ///
    /// Note that this is: the two pairs of end intervals overlap
    ///     and the amount and direction of overlap is the same for both.
    bool overlaps_or_adjacent(PackedEdge e) const {
        return packed_from().overlaps_or_adjacent(e.packed_from()) and
            packed_to().overlaps_or_adjacent(e.packed_to()) and
            packed_from().amount_of_overlap(e.packed_from()) ==
            packed_to().amount_of_overlap(e.packed_to());
    }


    /// Return the smallest edge containing *this and e
    ///
    /// Prereq: this->overlaps_or_adjacent(e)
    PackedEdge plus_overlapping_or_adjacent(PackedEdge e){
        auto f = packed_from().plus_overlapping_or_adjacent(e.packed_from());
        auto t = packed_to().plus_overlapping_or_adjacent(e.packed_to());
        return PackedEdge::from_fields
            (f.seqid_idx(), f.start(), f.length(), f.is_pos(),
             t.seqid_idx(), t.start(), t.is_pos());
    }


    bool operator<(const PackedEdge& e) const{
        using namespace PackedEdgeConstants;
        static_assert
            (bits_in_packed_edge - 64 <= numeric_limits<unsigned long>::digits,
             "Cannot use to_ulong to get the upper bits of a packed edge for comparison."
             "Not enough bits.");

        static_assert
            (64 <= numeric_limits<unsigned long long>::digits,
             "Not enough bits in unsigned long long to hold lower 64 bits of comparison");

        static_assert
            (64 + numeric_limits<unsigned long>::digits >= bits_in_packed_edge,
             "Need more than a ulong and ullong to hold all bits in a packed edge");

        auto b1 = (bits >> 64).to_ulong();
        auto b1e = (e.bits >> 64).to_ulong();
        return b1 < b1e or (b1 == b1e and
                            (bits & ones64).to_ullong() < (e.bits & ones64).to_ullong());
    }

    bool operator==(const PackedEdge& e) const {
        return bits == e.bits;
    }

    /// Return a PackedEdge where from is interchanged with to
    PackedEdge reverse() const {
        return PackedEdge::from_fields
            (to_seqid_idx(), to_start(), length(), to_is_pos(),
             from_seqid_idx(), from_start(), from_is_pos());
    }

    static PackedEdge from_fields(uint32_t from_seqid_idx,
                                  uint32_t from_start,
                                  uint32_t length,
                                  bool from_is_pos,
                                  uint32_t to_seqid_idx,
                                  uint32_t to_start,
                                  bool to_is_pos) {
        using namespace PackedEdgeConstants;

        PackedEdgeBits from_seqid_b(from_seqid_idx);
        PackedEdgeBits from_start_b(from_start);
        PackedEdgeBits from_is_pos_b(from_is_pos);
        PackedEdgeBits length_b(max_length - length);
        PackedEdgeBits to_seqid_b(to_seqid_idx);
        PackedEdgeBits to_start_b(to_start);
        PackedEdgeBits to_is_pos_b(to_is_pos);

        return PackedEdge{
            (length_b & ones18) << length_shift |
             from_seqid_b << from_seqid_shift |
             from_start_b << from_start_shift |
             from_is_pos_b << from_strand_shift |
             to_seqid_b << to_seqid_shift |
             to_start_b << to_start_shift |
             to_is_pos_b << to_strand_shift
        };
    }

    /// Make an edge from i->i
    static PackedEdge self_edge(PackedInterval i){
        return PackedEdge::from_fields
            (i.seqid_idx(), i.start(), i.length(), i.is_pos(),
             i.seqid_idx(), i.start(), i.is_pos());
    }
};

ostream& operator<<(ostream& out, PackedEdge e){
    return out << e.packed_from() << ".." << e.packed_to();
}

/// Return true if the empty iterval at the beginning of i (i's first
/// sliver) sorts before e's from interval's first sliver.
///
/// i.e i comes before e.from
inline bool first_pt_is_less(const PackedInterval i, const PackedEdge e){
    return make_tuple(i.seqid_idx(), i.start()) <
        make_tuple(e.from_seqid_idx(), e.from_start());
}

/// Return true if i's first sliver is the same as e's from interval's
/// first sliver
inline bool first_pt_is_same(const PackedInterval i, const PackedEdge e){
    return make_tuple(i.seqid_idx(), i.start()) ==
        make_tuple(e.from_seqid_idx(), e.from_start());
}


PackedEdge Edge::pack(const SeqIdTable& ids) const {
    if (from.length() != to.length()) {
        throw "Edge with different lengths " + to_string();
    }
    return PackedEdge::from_fields
        (seqid_index_of(ids, from.id),
         from.start,
         from.length(),
         from.strand == '+'?1:0,
         seqid_index_of(ids, to.id),
         to.start,
         to.strand == '+'?1:0);
}


PackedInterval Interval::pack(const SeqIdTable& ids) const {
    return PackedInterval::from
        (seqid_index_of(ids, id), start, length(), strand == '+'?1:0);
}

/// Return a new interval that covers both i1 and i2. They must
/// overlap and be on the same strand of the same sequence
PackedInterval combine_overlapping_intervals(PackedInterval i1, PackedInterval i2){
    auto new_start=min(i1.start(), i2.start());
    auto new_supremum=max(i1.start() + i1.length(), i2.start() + i2.length());
    return PackedInterval::from(
        i1.seqid_idx(),
        new_start,
        new_supremum - new_start,
        i1.is_pos());
}

struct OverlappingGroup {
    PackedInterval containing_interval;
    vector<PackedEdge> edges;

    /// Add the edge to the group if it's from interval overlaps containing_interval
    /// otherwise, return false and do nothing
    bool add(const PackedEdge& e) {

        if(containing_interval.overlaps_ignoring_strand(e.packed_from())){
            containing_interval = combine_overlapping_intervals
                (containing_interval, e.packed_from());
            edges.push_back(e);
            return true;
        }
        return false;
    }
};


/// Split edge on split_points and append the resulting edges onto dest
void split_edge_into(PackedEdge edge, const set<uint32_t>& split_points,
                     vector<PackedEdge>& dest) {
    PackedInterval from = edge.packed_from();
    set<uint32_t> intersecting_points;
    intersecting_points.insert(from.start());
    intersecting_points.insert(from.supremum());
    for(const auto& p: split_points) {
        if(from.contains(p)) {
            intersecting_points.insert(p);
        }
    }

    if(intersecting_points.size() < 2) {
        dest.push_back(edge);
        return;
    }

    // Pairs of points are the endpoints of the edges
    auto next = intersecting_points.begin();
    ++next;
    auto cur = intersecting_points.begin();
    for(; next != intersecting_points.end(); ++cur, ++next){
        uint32_t offset = *cur - edge.from_start();
        uint32_t length = *next - *cur;
        dest.push_back
            (PackedEdge::from_fields
             (edge.from_seqid_idx(),
              edge.from_start() + offset,
              length,
              edge.from_is_pos(),
              edge.to_seqid_idx(),
              edge.to_start() + offset,
              edge.to_is_pos()));
    }
}

/// Modify group.edges so that all from intervals are either disjoint
/// or identical
void split_edges_in_group(OverlappingGroup& group) {
    set<uint32_t> split_points;
    for(const auto& edge: group.edges) {
        split_points.insert(edge.from_start());
        split_points.insert(edge.from_start() + edge.length());
    }
    vector<PackedEdge> split_edges;
    for(const auto& e: group.edges) {
        split_edge_into(e, split_points, split_edges);
    }
    group.edges = split_edges;
}

/// edge: the edge in the cluster
///
/// from_point: the position on the from sequence that was used to
///     generate this cluster
///
/// to_point: the point on the to sequence that corresponds to from_point
///
/// parent_idx: the index in the cluster of the parent edge that
///     generated this edge. -1 for edges generated by the root interval.
///
struct ClusterEdge {
    PackedEdge edge;
    uint32_t from_point;
    uint32_t to_point;
    int parent_idx;

    static ClusterEdge from
        (PackedEdge edge, uint32_t from_point, int parent_idx) {
        return {edge, from_point,
                from_point + edge.to_start() - edge.from_start(),
                parent_idx};
    }
};

ostream& operator<<(ostream& out, ClusterEdge e){
    return out << "CE<<" << e.edge << ", " << e.from_point << "->"
               << e.to_point << ", " << e.parent_idx << ">>";
}

/// A cluster is a non-empty list of ClusterEdge objects returned by
/// edge_cluster_for(interval).
///
/// * All the parent_idx fields are accurate
///
/// * All edges with parent_idx == -1 have a from_ interval on the same
///   sequence
///
/// * There are no loops in the graph made by the edges even when
///   considered to connect only seq-ids
using EdgeCluster = vector<ClusterEdge>;

ostream& operator<<(ostream& out, EdgeCluster e){
    out << "[";
    for(auto it = e.begin(); out && it != e.end(); ++it){
        if(it != e.begin()){
            out << ",";
        }
        out << *it;
    }
    return out << "]";
}


/// Return a list of ClusterEdges whose edges contained images of point.
///
/// edges must have all its from intervals either disjoint or
/// identical ... no non-identical overlaps
///
/// The returned list is sorted for reproducibility
EdgeCluster edges_containing_point
(PackedInterval point, int parent_idx, const vector<PackedEdge>& edges){
    EdgeCluster cluster_edges;
    /// Calculate the list edges with overlapping from intervals. The
    /// list will be [start...supremum)
    ///
    /// Start the supremum at the first interval which starts at or
    /// after the point.
    auto supremum = upper_bound(edges.begin(), edges.end(), point,
                                first_pt_is_less);
    auto start = supremum;

    /// Move the supremum to be after all intervals which start at the
    /// search point. The ordering guarantees that no later intervals
    /// can contain this point. And the lack of empty intervals
    /// guarantees that all of those intervals do contain this point.
    while(supremum != edges.end() and first_pt_is_same(point, *supremum)) {
        ++supremum;
    }

    /// If the [start..supremum) interval is non-empty, we've found
    /// all the overlapping edges (since the "from" intervals of the
    /// edges are disjoint or identical)
    if(start != supremum) {
        /// Make cluster edges from the found edges and return them
        transform
            (start, supremum, back_inserter(cluster_edges),
             [point, parent_idx](const PackedEdge e) {
                return ClusterEdge::from(e, point.start(), parent_idx);
            });
        return cluster_edges;
    }

    /// The interval at the supremum did not contain the point. So,
    /// we check the interval before it.
    if(start == edges.begin()){
        // No interval before
        return cluster_edges;
    }
    --start;
    if(not start->packed_from().overlaps_ignoring_strand(point)) {
        /// The preceding interval does not contain the point, so we're in
        /// a gap. No matches
        return cluster_edges;
    }
    /// It did contain the point and all intervals are either
    /// disjoint or identical, so we can set the start to the last
    /// interval that matches the one we just found.
    PackedInterval containing_from = start->packed_from();
    while(start != edges.begin() and
          first_pt_is_same(containing_from, *prev(start))) {
        --start;
    }

    /// Make cluster edges from the found edges and return them
    transform
        (start, supremum, back_inserter(cluster_edges),
         [point, parent_idx](const PackedEdge e) {
            return ClusterEdge::from(e, point.start(), parent_idx);
        });
    return cluster_edges;
}

EdgeCluster edge_cluster_for
(PackedInterval interval, const vector<PackedEdge>& edges){
    // Remap by remapping the first point and creating an edge from that.
    PackedInterval point = PackedInterval::from
        (interval.seqid_idx(), interval.start(), 1, interval.is_pos());

    deque<ClusterEdge> waiting_edges;
    set<size_t> seen_seqids;
    auto add_unseen_edges = [&seen_seqids, &waiting_edges](auto new_edges){
        for(const ClusterEdge& ce: new_edges) {
            if(seen_seqids.count(ce.edge.to_seqid_idx()) == 0){
                waiting_edges.push_back(ce);
                seen_seqids.insert(ce.edge.to_seqid_idx());
                seen_seqids.insert(ce.edge.from_seqid_idx());
            }
        }
    };

    add_unseen_edges(edges_containing_point(point, -1, edges));
    EdgeCluster cluster_edges;
    while(not waiting_edges.empty()){
        ClusterEdge to_explore = waiting_edges.front();
        waiting_edges.pop_front();
        cluster_edges.push_back(to_explore);
        auto to_explore_pt = PackedInterval::from
            (to_explore.edge.to_seqid_idx(),
             to_explore.to_point,
             1,
             to_explore.edge.to_is_pos()); // strand should be ignored.
        add_unseen_edges
            (edges_containing_point
             (to_explore_pt, cluster_edges.size() - 1, edges));
    }

    if(cluster_edges.empty()) {
        // No alignments of first point in interval ... treat it as a PTLP
        // add an edge for its self aligment
        cluster_edges.push_back
            (ClusterEdge::from
             (PackedEdge::self_edge(point), point.start(), -1));
    }

    return cluster_edges;
}

// Given a cluster, return ptlp_path, ptlp_id
//
// ptlp_path is a list of ClusterEdge objects in the order they are traversed
//     to get to the ptlp from the initial sequence.
//
// ptlp_id is the SeqId of the Preferred Top Level Placement (PTLP) sequence
//     out of all the sequences in the cluster.
tuple<vector<ClusterEdge>, uint32_t> path_to_ptlp
(const EdgeCluster& cluster){
    set<uint32_t> seqid_indexes;
    auto from_id = [](const auto& ce){ return ce.edge.from_seqid_idx(); };
    auto to_id = [](const auto& ce){ return ce.edge.to_seqid_idx(); };
    auto sid_inserter = inserter(seqid_indexes, seqid_indexes.end());
    transform(cluster.begin(), cluster.end(), sid_inserter, from_id);
    transform(cluster.begin(), cluster.end(), sid_inserter, to_id);
    auto ptlp_id = *min_element(seqid_indexes.begin(), seqid_indexes.end());

    vector<ClusterEdge> ptlp_edges;
    copy_if
        (cluster.begin(), cluster.end(), back_inserter(ptlp_edges),
         [ptlp_id](const ClusterEdge& ce){
            return ce.edge.from_seqid_idx() == ptlp_id or
                 ce.edge.to_seqid_idx() == ptlp_id;
            });

    auto path_from_progenitor = [&cluster](ClusterEdge e) {
        vector<ClusterEdge> p{e};
        while(e.parent_idx != -1) {
            e = cluster[e.parent_idx];
            p.push_back(e);
        }
        reverse(p.begin(), p.end());
        return p;
    };
    auto index_of_ptlp = [ptlp_id](const ClusterEdge& e){
        if(e.edge.from_seqid_idx() == ptlp_id) {
            return 0;
        } else if(e.edge.to_seqid_idx() == ptlp_id) {
            return 1;
        } else {
            return 1024; /// Shouldn't ever happen...
        }
    };
    auto path_len = [&index_of_ptlp](const vector<ClusterEdge>& path){
        return (path.size()-1)*2 + index_of_ptlp(path.back());
    };

    // Return the shortest path to PTLP ... with one that starts at
    // the PTLP being shorter than one which has it at the end of the
    // first edge.
    vector<vector<ClusterEdge>> paths; paths.reserve(ptlp_edges.size());
    transform(ptlp_edges.begin(), ptlp_edges.end(), back_inserter(paths),
              path_from_progenitor);
    vector<ClusterEdge> ptlp_path = *min_element
        (paths.begin(), paths.end(),
         [&path_len](auto& p1, auto& p2) {
            return path_len(p1) < path_len(p2); });
    /// Detect if the path starts on the ptlp sequence ... and rewrite
    /// it so it maps to itself.
    if(path_len(ptlp_path) == 0){
        auto old_edge = ptlp_path[0];
        ptlp_path = vector<ClusterEdge>{ClusterEdge::from
            (PackedEdge::self_edge(old_edge.edge.packed_from()),
             old_edge.from_point, -1)};
    }

    return make_tuple(ptlp_path, ptlp_id);
}


/// Return new_bounds_start, new_bounds_supremum, offset, ptlp_is_pos
///
/// new_bounds_*: a pair of points giving the minimum and supremum values
///     in interval that align with the ptlp using the edges in
///     ptlp_path in order
///
/// offset: the number to add to a position in interval to get a
///     corresponding position on the ptlp sequence
///
/// ptlp_is_pos: true if a point in interval will be on in the
///     positive strand in the ptlp sequence
tuple<uint32_t, uint32_t, int32_t, bool> bounds_and_offset
(const vector<ClusterEdge>& ptlp_path, PackedInterval interval) {
    uint32_t new_bounds_start = interval.start(),
        new_bounds_supremum = interval.supremum();
    int32_t offset = 0;
    bool ptlp_is_pos = interval.is_pos();

    if(interval.seqid_idx() != ptlp_path.at(0).edge.from_seqid_idx()){
        throw "The interval to start computing bounds should be "
            "on the same sequence as the start of the path to "
            "the ptlp";
    }

    for(const ClusterEdge& e: ptlp_path) {
        /// Subtracting offset converts to coordinates back to
        /// interval coordinates. The current lower bound will get you
        /// to the "to" interval of the previous edge. The from
        /// interval of this edge is the same sequence (so has no new
        /// offset) but possibly different bounds. The lower bound
        /// can't decrease and so is the maximum of the current lower
        /// bound and the first point in the from_ interval when
        /// translated to original interval coordinates. Similarly for
        /// the upper bound, which can't increase.
        new_bounds_start = max(new_bounds_start, e.edge.from_start() - offset);
        new_bounds_supremum = min(new_bounds_supremum, e.edge.from_start() +
                                  e.edge.length() - offset);
        /// Here we update parameters to the "to" strand. Bounds stay
        /// the same because the two aligned intervals are the same
        /// length
        offset = offset + e.edge.to_start() - e.edge.from_start();
        if(e.edge.from_is_pos() != e.edge.to_is_pos()){
            ptlp_is_pos = not ptlp_is_pos;
        }
    }
    return make_tuple(new_bounds_start, new_bounds_supremum, offset,
                      ptlp_is_pos);
}

uint32_t num_ptlp_edges_calls{0};
/// Return the set edges representing the mappings of i to its PTLP(s)
vector<PackedEdge> ptlp_edges
(PackedInterval interval, const vector<PackedEdge>& edges){

    //auto n = num_ptlp_edges_calls.fetch_add(1, memory_order_relaxed);
    auto n = num_ptlp_edges_calls++;
    if(n % 1000 == 0){
        cerr << "Remapped " << n << " intervals at " << now_str() << endl;
    }

    vector<PackedEdge> new_edges;
    EdgeCluster cluster;
    vector<ClusterEdge> ptlp_path;
    uint32_t ptlp_id; //seqid_idx
    uint32_t new_bounds_start, new_bounds_supremum;
    int32_t offset;
    bool ptlp_is_pos;
    while(interval.length() > 0) {
        if(DEBUG){cerr << "About to remap " << interval << endl;}
        cluster = edge_cluster_for(interval, edges);
        if(DEBUG){
            cerr << "interval: " << interval << " yields cluster: " << cluster
                 << endl;
        }
        tie(ptlp_path, ptlp_id) = path_to_ptlp(cluster);
        if(DEBUG){
            cerr << "interval: " << interval << " yields ptlp: " << ptlp_id
                 << " and path: " << ptlp_path << endl; //DEBUG
        }
        tie(new_bounds_start, new_bounds_supremum, offset, ptlp_is_pos) =
            bounds_and_offset(ptlp_path, interval);
        if(DEBUG){
            cerr << "interval: " << interval << " yields bounds: ["
                 << new_bounds_start << ", " << new_bounds_supremum
                 << ") offset: " << offset << " ptlp_strand: "
                 << (ptlp_is_pos?'+':'-') << endl;
        }
        auto new_edge = PackedEdge::from_fields
             (interval.seqid_idx(), new_bounds_start,
              new_bounds_supremum - new_bounds_start, interval.is_pos(),
              ptlp_id, new_bounds_start + offset, ptlp_is_pos);
        new_edges.push_back(new_edge);

        interval = PackedInterval::from
            (interval.seqid_idx(), new_bounds_supremum,
             interval.length() - new_edge.length(), interval.is_pos());
    }

    return new_edges;
}

/// Return the set of intervals that the given point maps to
vector<PackedInterval> remap_point_to_all
(PackedInterval point, const vector<PackedEdge>& edges){

    set<PackedInterval> new_points;
    EdgeCluster cluster;

    if(DEBUG){cerr << "About to remap " << point << endl;}
    cluster = edge_cluster_for(point, edges);
    if(DEBUG){
        cerr << "point: " << point << " yields cluster: " << cluster
             << endl;
    }
    new_points.insert(point);
    for(auto it = cluster.rbegin(); it != cluster.rend(); ++it){
        // Follow the path to the base edge found from the original point
        const auto& ce = *it;
        auto* cur = &ce;
        while(cur->parent_idx != -1){
            cur = &cluster[cur->parent_idx];
        }
        bool base_is_same_strand =
            cur->edge.from_is_pos() == ce.edge.to_is_pos();
        // Since I'm only remapping a point, I can just use the
        // to_point ... it embodies the remapping already.
        new_points.insert
            (PackedInterval::from
             (ce.edge.to_seqid_idx(), ce.to_point, 1,
              base_is_same_strand?point.is_pos():!point.is_pos()));
    }

    return vector<PackedInterval>{new_points.begin(), new_points.end()};
}


/// Split edges so that "from" intervals are either disjoint or identical
///
/// Result is sorted and without duplicate edges.
void split_overlapping_edges(vector<PackedEdge>& packed_edges) {
    cerr << "Grouping overlapping edges: " << now_str() << endl;
    vector<OverlappingGroup> overlapping;
    for(const auto& edge: packed_edges){
        if(overlapping.empty() or not overlapping.back().add(edge)){
            overlapping.emplace_back(OverlappingGroup{
                    edge.packed_from(), vector<PackedEdge>{edge}});
        }
    }
    cerr << "Done. (" << overlapping.size() << " groups.)"  << endl;
    cerr << "Splitting edges in groups & flattening groups: "
         << now_str() << endl;
    par::for_each(overlapping.begin(), overlapping.end(), split_edges_in_group);
    // Flatten and remove duplicates
    {
        packed_edges.clear();
        for(const auto& group: overlapping) {
            for(const auto& edge: group.edges) {
                packed_edges.push_back(edge);
            }
        }
        par::sort(packed_edges.begin(), packed_edges.end());
        auto new_end = unique(packed_edges.begin(), packed_edges.end());
        packed_edges.erase(new_end, packed_edges.end());
    }
    cerr << "Done (" << packed_edges.size() << " edges after splitting.)"
         << endl;
}
