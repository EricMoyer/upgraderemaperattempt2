#include "c_packed_edge_remapping_lib.hpp"

int main(int argc, char**argv){
    if(argc < 2) {
        cerr << "Usage: cat lines.ads.txt | test_remap_to_all "
            "interval_to_remap" << endl
            << endl
            << "Interval format NC_000001.1:100:+:10 for a 10 nt interval "
            "starting at position 100" << endl
            << "Not sure how the code will work for empty intervals";
        exit(-1);
    }

    auto to_remap_unpacked = Interval::from_string(argv[1]);

    if(to_remap_unpacked.length() != 1){
        cerr << "Full intervals unimplemented. Can only "
             << "remap points now" << endl;
        exit(-2);
    }

    /// Read the lines from stdin into the lines vector
    vector<string> lines;
    {
        /// Read from first argument if given or from stdin
        string line;
        cerr << "Reading ... (" << now_str() << ")" << endl;
        while(getline(cin, line)){
            if (lines.size() % 1000000 == 0){
                cerr << "Read " << lines.size() << " lines" << endl;
            }
            lines.push_back(line);
        }
        cerr << "Done. (Read " << lines.size() << " lines)" << endl;
    }

    // Transform each line into a PackedEdge
    cerr << "Make seqids" << endl;
    SeqIdTable ids;
    {
        set<SeqId> ids_set;
        for(const auto& line: lines){
            const auto& e = Edge::from_string(line);
            ids_set.insert(e.from.id);
            ids_set.insert(e.to.id);
        }
        ids.resize(ids_set.size());
        copy(ids_set.begin(), ids_set.end(), ids.begin());
    }
    cerr << "Done." << endl;

    if(DEBUG){
        cerr << "SeqIds: " << endl; //DEBUG
        for(auto id: ids){ //DEBUG
            cerr << id.to_string() << endl; //DEBUG
        } //DEBUG
        cerr << "Done" << endl; //DEBUG
    }

    cerr << "Make packed edge objects" << endl;
    vector<PackedEdge> packed_edges(lines.size());
    {
        par::transform
            (lines.begin(), lines.end(), packed_edges.begin(),
             [&ids](const auto& line){
                return Edge::from_string(line).pack(ids); });
        lines.resize(0); // Don't need the lines anymore, free up the memory.
        par::sort(packed_edges.begin(), packed_edges.end());
        auto new_end = unique(packed_edges.begin(), packed_edges.end());
        packed_edges.erase(new_end, packed_edges.end());
    }
    cerr << "Done (" << packed_edges.size() << " unique edges.)" << endl;

    auto to_remap = to_remap_unpacked.pack(ids);

    constexpr auto num_remaps = 100000;
    cerr << "Start remapping test: " << num_remaps << " at " << now_str() << endl;
    for(auto i = num_remaps + 1; i != 0; --i){
        remap_point_to_all(to_remap, packed_edges);
    }
    cerr << "Finished remapping test: " << num_remaps << " at " << now_str() << endl;

    for(auto i: remap_point_to_all(to_remap, packed_edges)){
        cout << i.unpack(ids).to_string() << endl;
    }

    return 0;
}
