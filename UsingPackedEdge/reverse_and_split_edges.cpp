#include "c_packed_edge_remapping_lib.hpp"

int main(int argc, char**argv){
    /// Read the input into the lines vector
    vector<string> lines;
    {
        /// Read from first argument if given or from stdin
        istream* in = nullptr;
        unique_ptr<ifstream> fin;
        if(argc > 1){
            fin.reset(new ifstream(argv[1]));
            in = fin.get();
        } else {
            in = &cin;
        }

        string line;
        cerr << "Reading ... (" << now_str() << ")" << endl;
        while(getline(*in, line)){
            if (lines.size() % 1000000 == 0){
                cerr << "Read " << lines.size() << " lines" << endl;
            }
            lines.push_back(line);
        }
        cerr << "Done. (Read " << lines.size() << " lines)" << endl;
    }

    // Transform each line into a PackedEdge
    cerr << "Make seqids" << endl;
    SeqIdTable ids;
    {
        set<SeqId> ids_set;
        for(const auto& line: lines){
            const auto& e = Edge::from_string(line);
            ids_set.insert(e.from.id);
            ids_set.insert(e.to.id);
        }
        ids.resize(ids_set.size());
        copy(ids_set.begin(), ids_set.end(), ids.begin());
    }
    cerr << "Done." << endl;

    if(DEBUG){
        cerr << "SeqIds: " << endl; //DEBUG
        for(auto id: ids){ //DEBUG
            cerr << id.to_string() << endl; //DEBUG
        } //DEBUG
        cerr << "Done" << endl; //DEBUG
    }

    cerr << "Make reverse packed edge objects" << endl;
    vector<PackedEdge> packed_edges(lines.size());
    {
        par::transform
            (lines.begin(), lines.end(), packed_edges.begin(),
             [&ids](const auto& line){
                return Edge::from_string(line).pack(ids).reverse(); });
        lines.resize(0); // Don't need the lines anymore, free up the memory.
        par::sort(packed_edges.begin(), packed_edges.end());
        auto new_end = unique(packed_edges.begin(), packed_edges.end());
        packed_edges.erase(new_end, packed_edges.end());
    }
    cerr << "Done (" << packed_edges.size() << " unique edges.)" << endl;

    // Split edges so that "from" intervals are either disjoint or identical
    split_overlapping_edges(packed_edges);

    if(DEBUG){
        cerr << "Split edges:" << endl;
        for(auto e: packed_edges){
            cerr << "Edge:" << e << endl;
        }
        cerr << "Done." << endl;
    }


    cerr << "Writing out results: " << now_str() << endl;
    for(const auto e: packed_edges){
        cout << e.unpack(ids).to_string() << endl;
    }
    cerr << "Done" << endl;

    return 0;
}
