#!/usr/bin/env python3
import sys
import itertools

# Extract sequence accessions from edges on stdin and output a grep command that will extract them from a file
pairs = [line.split('\t') for line in sys.stdin]
seqs = [(p[0].split(':')[0],p[1].split(':')[0]) for p in pairs]
acc_pairs = [(s[0].split('.')[0],s[1].split('.')[0]) for s in seqs]
accs = set(itertools.chain.from_iterable(acc_pairs))

print('grep ',"|".join(sorted(accs)))

